const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config');

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
  stats: 'errors-only',
  proxy: {
    '/api/v1/*': {
      target: 'http://localhost:9070',
      secure: false
    },
    '/v/*': {
      target: 'http://localhost:9070',
      secure: false
    },
    '/login': {
      target: 'http://localhost:9070',
      secure: false
    },
    '/logout': {
      target: 'http://localhost:9070',
      secure: false
    }
  }
}).listen(3000, 'localhost', (err) => {
  if (err) {
    console.log(err); // eslint-disable-line
  }

  console.log('Listening at localhost:3000'); // eslint-disable-line
});
