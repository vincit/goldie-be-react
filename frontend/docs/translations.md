# Translations
--------------

### Used packages

In the project we are using [i18next](http://i18next.com/docs/) and [18next-react](https://github.com/i18next/react-i18next) for localization support. It handles localization well and the translations can be loaded from backend or cache easily. The users current language is also stored in a cookie and i18next can load it automatically from there.

### How to add translations

i18next uses namespaces for translations. To add a new namespace, create a ```*.json``` - file to the corresponding language folders in ```18n``` - folder.

Example ```18n/fi/name.json```:

```
{
    "name": {
        "firstName": "Etunimi",
        "lastName": "Sukunimi"
    }
}
```

JSON-files need to be imported somewhere, so Webpack as the buildtool can find the json files and include them in the bundle. Namespaces also need to be declared in the i18next -config in ```ì18n/index.js```. Check the file for more details.

### How to use translation files

To use this translation-file in a React component, you need to wrap the component in a translate higher order component, for example:

```export default translate(['login'], { wait: true })(Login);```

where the array takes a list of namespaces and the ```{ wait: true }``` part will delay rendering until the translations are loaded. 

To include the translated text, translate hoc adds a prop called ```t```, for translating given texts.

Example render-method:

```
render() {
    const { t } = this.props;
    
    return (
        <label>{t('loginForm.username')}</label>
    );
```  