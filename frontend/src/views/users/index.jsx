import { connect } from 'react-redux';
import Dimensions from 'react-dimensions';
import Users from './Users';
import {
  requestUserData,
  requestSingleUser
} from './actions';

import './users.scss';

const mapStateToProps = state => ({
  users: state.getIn(['rootReducer', 'userData', 'users', 'content']).toJS(),
  totalUsers: state.getIn(['rootReducer', 'userData', 'users', 'total']),
  pageable: state.getIn(['rootReducer', 'userData', 'users', 'pageable'])
});

const mapDispatchToProps = dispatch => ({
  requestUserData(queryParams, onSuccess) {
    dispatch(requestUserData({ data: queryParams, onSuccess }));
  },
  requestSingleUser(queryParams) {
    dispatch(requestSingleUser(queryParams));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dimensions()(Users));
