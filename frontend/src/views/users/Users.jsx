import React, { Component, PropTypes } from 'react';
import { Pagination } from 'react-bootstrap';

import UsersTable from '../../components/Tables/UsersTable';

const PAGE_SIZE = 20;

class Users extends Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      size: PAGE_SIZE,
      sort: 'username',
      type: 'page'
    };
  }

  componentDidMount() {
    this.props.requestUserData(this.getQueryParams());
  }

  getQueryParams() {
    return {
      page: this.state.page,
      size: PAGE_SIZE,
      sort: 'username',
      type: 'page'
    };
  }

  handlePageSelect(selectedPage) {
    const queryPage = selectedPage - 1;
    const queryParameters = this.getQueryParams();
    queryParameters.page = queryPage;
    this.props.requestUserData(queryParameters, () => this.setState({ page: queryPage }));
  }

  render() {
    const { users, totalUsers } = this.props;
    const totalPages = Math.ceil(totalUsers / PAGE_SIZE);
    const activePage = this.state.page + 1;

    const pagination = totalPages > 1 ? (
      <Pagination
        prev
        next
        first
        last
        ellipsis
        boundaryLinks
        items={totalPages}
        maxButtons={5}
        activePage={activePage}
        onSelect={selectedPage => this.handlePageSelect(selectedPage)}
      />
    ) : null;

    return (
      <div>
        {pagination}
        <UsersTable
          users={users}
          width={this.props.containerWidth}
        />
        {pagination}
      </div>
    );
  }
}

Users.propTypes = {
  containerWidth: PropTypes.number.isRequired,
  requestUserData: PropTypes.func.isRequired,
  users: PropTypes.array,
  totalUsers: PropTypes.number
};

export default Users;
