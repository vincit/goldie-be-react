/* eslint-env node, mocha */
/* eslint no-console: 0*/

import { expect } from 'chai';
import Immutable from 'immutable';

import {
  requestUserDataSuccess,
  requestUserDataFailure,
  requestDeleteUserSuccess,
  requestDeleteUserFailure,
  requestUpdateUserSuccess,
  requestUpdateUserFailure
} from '../actions';
import { userDataReducer } from '../reducers';

describe('userdata reducer', () => {
  const initialState = {
      content: Immutable.List([]),
      pageable: null,
      total: null
  };

  it('should return the initial state correctly', () => {
    expect(
      userDataReducer(undefined, { type: ''}).equals(Immutable.Map(initialState))
    ).to.equal(true);
  });

  describe('get user data', () => {
    it('should handle REQUEST_USER_DATA_SUCCESS and return a List with users', () => {
      const startingState = Immutable.Map(initialState);
      const users = Immutable.List([{
        id: 1,
        username: 'user1'
      }, {
        id: 2,
        username: 'user2'
      }]);
      const action = {
        content: [{
          id: 1,
          username: 'user1'
        }, {
          id: 2,
          username: 'user2'
        }],
        pageable: null,
        total: null
      };
      const expectedState = Immutable.Map(initialState).merge({ content: users});

      const reducer = JSON.stringify(
        userDataReducer(startingState, requestUserDataSuccess(action)).toJS());
      const expected = JSON.stringify(expectedState.toJS());

      expect(reducer === expected).to.equal(true);
    });

    it('should handle REQUEST_USER_DATA_FAILURE and return an empty List', () => {
      const startingState = Immutable.Map(initialState);
      const expectedState = Immutable.Map(initialState);

      expect(
        userDataReducer(startingState, requestUserDataFailure()).equals(expectedState)
      ).to.equal(true);
    });
  });

  describe('delete a single user', () => {
    it('should handle REQUEST_DELETE_USER_SUCCESS and return a List without the user', () => {
      const startingState = Immutable.Map({
        content: Immutable.List([
          {
            id: 1,
            username: 'user1'
          }, {
            id: 2,
            username: 'user2'
          }]),
        pageable: null,
        total: 2
      });

      const expectedState = Immutable.Map({
        content: Immutable.List([
          {
            id: 2,
            username: 'user2'
          }]),
        pageable: null,
        total: 1
      });

      const action = {
        id: 1
      };

      const reducer = JSON.stringify(
          userDataReducer(startingState, requestDeleteUserSuccess(action)).toJS());
      const expected = JSON.stringify(expectedState.toJS());

      expect(reducer === expected).to.equal(true);
    });
  });

  it('should handle REQUEST_DELETE_USER_FAILURE and return the same state', () => {
    const startingState = Immutable.List([{
      id: 1,
      username: 'user1'
    }, {
      id: 2,
      username: 'user2'
    }]);

    expect(
      userDataReducer(startingState, requestDeleteUserFailure()).equals(startingState)
    ).to.equal(true);
  });

  describe('update a single user', () => {
    it('should handle REQUEST_UPDATE_USER_SUCCESS and return a List with the updated user', () => {
      const startingState = Immutable.Map({
        content: Immutable.List([{
          id: 1,
          username: 'user1',
          role: 'ROLE_ADMIN'
        }, {
          id: 2,
          username: 'user2',
          role: 'ROLE_USER'
        }]),
        pageable: null,
        total: null
      });

      const expectedState = Immutable.Map({
        content: Immutable.List([{
          id: 1,
          username: 'user1',
          role: 'ROLE_ADMIN'
        }, {
          id: 2,
          username: 'newusername',
          role: 'ROLE_ADMIN'
        }]),
        pageable: null,
        total: null
      });

      const action = {
        id: 2,
        username: 'newusername',
        role: 'ROLE_ADMIN'
      };

      const reducer = JSON.stringify(
          userDataReducer(startingState, requestUpdateUserSuccess(action)).toJS());
      const expected = JSON.stringify(expectedState.toJS());

      expect(reducer === expected).to.equal(true);
    });

    it('should handle REQUEST_UPDATE_USER_FAILURE and return the same state', () => {
      const startingState = Immutable.List([{
        id: 1,
        username: 'user1'
      }, {
        id: 2,
        username: 'user2'
      }]);
      const action = {
        validationErrors: [{
          errorMessage: ''
        }]
      };

      expect(
        userDataReducer(startingState, requestUpdateUserFailure(action)).equals(startingState)
      ).to.equal(true);
    });
  });
});
