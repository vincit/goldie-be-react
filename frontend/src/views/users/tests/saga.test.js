/* eslint-env node, mocha */
/* eslint no-console: 0*/

import { call, put } from 'redux-saga/effects';
import { expect } from 'chai';
import sinon from 'sinon';

import {
requestUsers,
    requestUser,
    requestUpdateSingleUser,
    requestDeleteSingleUser
    } from '../sagas';
import {
requestUserDataSuccess,
    requestUserDataFailure,
    requestSingleUserSuccess,
    requestSingleUserFailure,
    requestUpdateUserSuccess,
    requestUpdateUserFailure,
    requestDeleteUserSuccess,
    requestDeleteUserFailure
    } from '../actions';
import Api from '../../../api';

describe('users saga', () => {
  describe('on get userdata, the generator', () => {
    const action = {
      payload: {
        data: {
          page: 0,
          size: 20,
          sort: 'username',
          type: 'page'
        }
      }
    };
    const generator = requestUsers(action);

    it('should call userdata api', () => {
      expect(generator.next().value).to.deep.equal(call(Api.users.fetchUserData, action.payload.data));
    });

    it('and then should dispatch REQUEST_USER_DATA_SUCCESS action', () => {
      const response = {
        data: {
          content: [{
              id: 1,
              username: 'user1'
            }, {
              id: 2,
              username: 'user2'
            }]
        }
      };

      expect(generator.next(response).value).to.deep.equal(
          put(requestUserDataSuccess(response.data)));
    });
  });

  describe('on get userdata error, the generator', () => {
    const action = {
      payload: {
        page: 0,
        size: 20,
        sort: 'username',
        type: 'page'
      }
    };
    const generator = requestUsers(action);
    generator.next();

    it('should dispatch REQUEST_USER_DATA_FAILURE action', () => {
      const error = {
        error: 'error'
      };
      expect(generator.throw(error).value).to.deep.equal(put(requestUserDataFailure(error)));
    });
  });

  describe('on get single user, the generator', () => {
    const action = {
      payload: {
        id: 1
      }
    };
    const generator = requestUser(action);

    it('should call user api', () => {
      expect(generator.next().value).to.deep.equal(call(Api.users.fetchSingleUser, action.payload));
    });

    it('and then should dispatch REQUEST_SINGLE_USER_SUCCESS action', () => {
      const response = {
        data: {
          id: 1,
          username: 'user1'
        }
      };

      expect(generator.next(response).value).to.deep.equal(
          put(requestSingleUserSuccess(response.data)));
    });
  });

  describe('on get single user error, the generator', () => {
    const action = {
      payload: {
        id: 1
      }
    };
    const generator = requestUser(action);
    generator.next();

    it('should dispatch REQUEST_SINGLE_USER_FAILURE action', () => {
      const error = {
        error: 'error'
      };
      expect(generator.throw(error).value).to.deep.equal(put(requestSingleUserFailure(error)));
    });
  });

  describe('on update single user, the generator', () => {
    const onSuccessSpy = sinon.spy();
    const action = {
      payload: {
        userData: {
          id: 1,
          username: 'user1',
          role: 'ROLE_ADMIN'
        },
        onSuccess: onSuccessSpy
      }
    };
    const generator = requestUpdateSingleUser(action);

    it('should call update user api', () => {
      expect(generator.next().value).to.deep.equal(call(Api.users.updateUser,
          action.payload.userData));
    });

    it('and then should dispatch REQUEST_UPDATE_USER_SUCCESS action', () => {
      const response = {
        data: {
          id: 1,
          username: 'user1',
          role: 'ROLE_ADMIN'
        }
      };

      expect(generator.next(response).value).to.deep.equal(
          put(requestUpdateUserSuccess(response.data)));

    });

    it('and then yield a call for onSuccess', () => {
      expect(generator.next().value).to.deep.equal(call(onSuccessSpy));
    })
  });

  describe('on update single user error, the generator', () => {
    const action = {
      payload: {
        id: 1,
        username: 'user1',
        role: 'ROLE_ADMIN'
      }
    };
    const generator = requestUpdateSingleUser(action);
    generator.next();

    it('should dispatch REQUEST_UPDATE_USER_FAILURE action', () => {
      const error = {
        data: {
          validationErrors: [{
              errorMessage: ''
            }]
        }
      };
      expect(generator.throw(error).value).to.deep.equal(put(requestUpdateUserFailure(error.data)));
    });
  });

  describe('on delete single user, the generator', () => {
    const action = {
      payload: {
        id: 1
      }
    };
    const generator = requestDeleteSingleUser(action);

    it('should call delete user api', () => {
      expect(generator.next().value).to.deep.equal(call(Api.users.deleteUser, action.payload.id));
    });

    it('and then should dispatch REQUEST_DELETE_USER_SUCCESS action', () => {
      const response = {
        data: {
          id: 1
        }
      };

      expect(generator.next(response).value).to.deep.equal(
          put(requestDeleteUserSuccess(response.data)));
    });
  });

  describe('on delete single user error, the generator', () => {
    const action = {
      payload: {
        id: 1
      }
    };
    const generator = requestDeleteSingleUser(action);
    generator.next();

    it('should dispatch REQUEST_DELETE_USER_FAILURE action', () => {
      const error = {
        error: 'error'
      };
      expect(generator.throw(error).value).to.deep.equal(put(requestDeleteUserFailure(error)));
    });
  });
});
