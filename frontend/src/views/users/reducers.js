/* global _ */
import { combineReducers } from 'redux-immutable';
import * as _ from 'lodash';
import Immutable from 'immutable';
import { handleActions } from 'redux-actions';
import {
  requestUserDataSuccess,
  requestUserDataFailure,
  requestSingleUserSuccess,
  requestSingleUserFailure,
  requestUpdateUserSuccess,
  requestUpdateUserFailure,
  requestDeleteUserSuccess,
  requestDeleteUserFailure,
  setCurrentUser
} from './actions';

const initialUserDataState = {
  content: Immutable.List([]),
  pageable: null,
  total: null
};

const handleUserDataSuccess = (state, action) => (Immutable.Map({
  content: Immutable.List(action.payload.content),
  pageable: action.payload.pageable,
  total: action.payload.total
}));

export const userDataReducer = handleActions({
  [requestUserDataSuccess]: handleUserDataSuccess,
  [requestUserDataFailure]: () => {
    alert('Käyttäjätietojen haku epäonnistui'); // eslint-disable-line
    return Immutable.Map(initialUserDataState);
  },
  [requestDeleteUserSuccess]: (state, action) => {
    const userList = Immutable.List(state.get('content'));
    const currentTotal = state.get('total');
    const foundUserIndex = userList.findIndex(user => user.id === action.payload.id);
    return state.merge({ content: userList.delete(foundUserIndex), total: currentTotal - 1 });
  },
  [requestDeleteUserFailure]: (state) => {
    alert('Käyttäjän poistaminen epäonnistui'); // eslint-disable-line
    return state;
  },
  [requestUpdateUserSuccess]: (state, action) => {
    const userList = Immutable.List(state.get('content'));
    const foundUserIndex = userList.findIndex(user => user.id === action.payload.id);
    return state.merge({ content: userList.update(foundUserIndex, () => action.payload) });
  },
  [requestUpdateUserFailure]: (state, action) => {
    const validationErrors = action.payload.validationErrors ?
      _.map(action.payload.validationErrors, 'errorMessage') : '';
    alert(`Käyttäjän päivittäminen epäonnistui ${JSON.stringify(validationErrors)}`); // eslint-disable-line
    return state;
  }
}, Immutable.Map(initialUserDataState));

const initialCurrentUserState = [];

export const currentUserReducer = handleActions({
  [requestSingleUserSuccess]: (state, action) => Immutable.Map(action.payload),
  [requestSingleUserFailure]: () => {
    alert('Käyttäjän haku epäonnistui'); // eslint-disable-line
    return Immutable.Map(initialCurrentUserState);
  },
  [setCurrentUser]: (state, action) => Immutable.Map(action.payload)
}, Immutable.Map(initialCurrentUserState));

export default combineReducers({
  users: userDataReducer,
  currentUser: currentUserReducer
});
