import React from 'react';
import { IndexRoute, Route }
from 'react-router';
import { userIsAuthenticated }
from '../../../routes/route.authentication';
import Home from './..';

const HomeRoutes = (
  <Route path="home">
    <IndexRoute component={userIsAuthenticated(Home)} />
  </Route>
);

export default HomeRoutes;
