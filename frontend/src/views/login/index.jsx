import { connect } from 'react-redux';
import Login from './Login';
import { loginUser, resetValidationState } from './actions';

import './login.scss';

const mapStateToProps = state => ({
  isAuthenticated: state.getIn(['rootReducer', 'auth', 'isAuthenticated']),
  error: state.getIn(['rootReducer', 'auth', 'loginFailedError'])
});

const mapDispatchToProps = dispatch => ({
  login(user) {
    dispatch(loginUser(user));
  },
  resetValidationState() {
    dispatch(resetValidationState());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
