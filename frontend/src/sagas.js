import loginSagas from './views/login/sagas';
import forgotPasswordSagas from './views/forgotpassword/sagas';
import usersSagas from './views/users/sagas';

export default function* rootSaga() {
  yield [
    ...loginSagas,
    ...forgotPasswordSagas,
    ...usersSagas
  ];
}
