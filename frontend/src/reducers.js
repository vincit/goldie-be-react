import { combineReducers } from 'redux-immutable';

import authReducer from './views/login/reducers';
import forgotPasswordReducer from './views/forgotpassword/reducers';
import userReducer from './views/users/reducers';

export default combineReducers({
  auth: authReducer,
  forgotPassword: forgotPasswordReducer,
  userData: userReducer
});
