import axios from 'axios';

const authentication = {
  login: (payload) => {
    const data = `username=${encodeURIComponent(payload.username)}&password=${encodeURIComponent(payload.password)}&remember_me=${payload.rememberMe}`; // eslint-disable-line
    return axios.post('/login', data, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
  },
  logout: () => axios.post('/logout', {}),
  authenticate: () => axios.get('/api/v1/account')
};

export default authentication;
