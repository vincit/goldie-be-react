import React from 'react';
import { render } from 'react-dom';

import Root from './components/Root';
import './styles/app.scss';
import configureStore from './store';

const store = configureStore();

render(
  <Root store={store} />,
  document.getElementById('app-container')
);

// This is for react-hot-loader, (should) update changes on the fly
if (module.hot) {
  module.hot.accept('./components/Root', () => {
    const NextRoot = require('./components/Root').default; // eslint-disable-line
    render((
      <NextRoot store={store} />
    ), document.getElementById('app-container'));
  });
}
