import i18next from 'i18next';
import XHR from 'i18next-xhr-backend';
import LngDetector from 'i18next-browser-languagedetector';

export default i18next
  .use(XHR)
  .use(LngDetector)
  .init({
    detection: {
      order: ['cookie'],
      caches: ['cookie'], // Cache user language to co
      cookieMinutes: 40320 // One month
    },
    // List the namespaces aka *.json files
    ns: [
      'common',
      'menu',
      'login',
      'forgotpassword',
      'forms',
      'tables',
      'modals'
    ],
    debug: false,
    defaultNS: 'common',
    fallbackLng: 'fi',
    backend: {
      loadPath: '/i18n/{{lng}}/{{ns}}.json'
    }
  });
