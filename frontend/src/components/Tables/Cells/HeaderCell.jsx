import React, { PropTypes } from 'react';
import { Cell } from 'fixed-data-table';
import { translate } from 'react-i18next';

const HeaderCell = ({ translationKey, t }) => (
  <Cell className="headercell">
    {t(translationKey)}
  </Cell>
);

HeaderCell.propTypes = {
  translationKey: PropTypes.string.isRequired,
  t: PropTypes.func.isRequired
};

export default translate(['tables'], { wait: true })(HeaderCell);
