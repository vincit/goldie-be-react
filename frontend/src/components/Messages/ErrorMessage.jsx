import React, { PropTypes } from 'react';
import { Alert } from 'react-bootstrap';

const ErrorMessage = ({ isVisible, children }) => {
  if (isVisible) {
    return (
      <Alert bsStyle="danger">
        {children}
      </Alert>
    );
  }
  return null;
};

ErrorMessage.propTypes = {
  isVisible: PropTypes.bool,
  children: PropTypes.node.isRequired
};

export default ErrorMessage;
