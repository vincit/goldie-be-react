import { connect } from 'react-redux';
import EditUserForm from './EditUserForm';

const mapStateToProps = state => ({
  initialValues: state.getIn(['rootReducer', 'userData', 'currentUser'])
});

export default connect(
  mapStateToProps
)(EditUserForm);
