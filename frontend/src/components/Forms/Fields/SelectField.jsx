import React from 'react';
import { ControlLabel, FormControl, HelpBlock, FormGroup } from 'react-bootstrap';

const SelectField = field => (
  <FormGroup
    controlId={field.input.placeholder}
    validationState={(field.meta.invalid && field.meta.error) ? 'error' : 'success'}
  >
    <ControlLabel>{field.placeholder}</ControlLabel>
    <FormControl {...field.input} componentClass="select">
      {field.options}
    </FormControl>
    {
      (field.meta.invalid && field.meta.error) ?
        <HelpBlock>{field.meta.error}</HelpBlock> : null
    }
  </FormGroup>
);

export default SelectField;
