import React from 'react';
import { ControlLabel, FormControl, HelpBlock, FormGroup } from 'react-bootstrap';

const InputField = field => (
  <FormGroup
    controlId={field.input.placeholder}
    validationState={(field.meta.invalid && field.meta.error) ? 'error' : 'success'}
  >
    <ControlLabel>{field.placeholder}</ControlLabel>
    <FormControl {...field.input} type={field.type} placeholder={field.placeholder} />
    {
      (field.meta.invalid && field.meta.error) ?
        <HelpBlock>{field.meta.error}</HelpBlock> : null
    }
  </FormGroup>
);

export default InputField;
