/* eslint-disable */
import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Button, Checkbox, ControlLabel, FormControl, FormGroup } from 'react-bootstrap';
import { Link } from 'react-router';
import { translate } from 'react-i18next';

const styles = {
  paddingBottom: '150px'
};

class LoginForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoginBtnDisabled: true
    };
  }

  getFieldValues() {
    return {
      username: ReactDOM.findDOMNode(this.refs.username).value,
      password: ReactDOM.findDOMNode(this.refs.password).value
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    const { username, password } = this.getFieldValues();
    const rememberMe = this.rememberMeRef.checked;
    this.props.onSubmit({ username, password, rememberMe });
  }

  handleChange() {
    const { username, password } = this.getFieldValues();

    (username.length > 0 && password.length > 0) ? // eslint-disable-line
      this.setState({ isLoginBtnDisabled: false }) :
      this.setState({ isLoginBtnDisabled: true });
  }

  render() {
    const { t } = this.props;

    return (
      <form style={styles} onSubmit={e => this.handleSubmit(e)}>
        <FormGroup controlId="username">
          <ControlLabel>{t('loginForm.username')}</ControlLabel>
          <FormControl
            type="text"
            placeholder={t('loginForm.usernamePlaceholder')}
            ref="username"
            onChange={() => this.handleChange()}
          />
        </FormGroup>
        <FormGroup controlId="password">
          <ControlLabel>{t('loginForm.password')}</ControlLabel>
          <FormControl
            type="password"
            placeholder={t('loginForm.passwordPlaceholder')}
            ref="password"
            onChange={() => this.handleChange()}
          />
        </FormGroup>
        <FormGroup className="formactiongroup">
          <Checkbox
            name="rememberme"
            inputRef={ref => { this.rememberMeRef = ref }}
          >
            {t('loginForm.rememberMe')}
          </Checkbox>
          <Link to="forgot">{t('loginForm.forgotPassword')}</Link>
        </FormGroup>
        <Button
          bsStyle="primary"
          className="btn-block"
          type="submit"
          disabled={this.state.isLoginBtnDisabled}
        >
          {t('loginForm.loginBtnText')}
        </Button>
      </form>
    );
  }
}

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

export default translate(['login'], { wait: true })(LoginForm);
export { LoginForm as PureLoginForm };
