import { connect } from 'react-redux';
import Navigation from './Navigation';
import { logoutUser } from '../../views/login/actions';

const mapStateToProps = state => ({
  isAuthenticated: state.getIn(['rootReducer', 'auth', 'isAuthenticated']),
  role: state.getIn(['rootReducer', 'auth', 'role'])
});

const mapDispatchToProps = dispatch => ({
  logout() {
    dispatch(logoutUser());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navigation);
