import React, { PropTypes } from 'react';
import { Provider } from 'react-redux';
import { Router, Route, Redirect, browserHistory } from 'react-router';
import { I18nextProvider } from 'react-i18next';
import { AppContainer } from 'react-hot-loader'; // eslint-disable-line

import i18n from '../../i18n';

import App from './../../views/app';
import Login from './../../views/login';
import ForgotPassword from './../../views/forgotpassword';
import HomeRoutes from './../../views/home/routes';
import UsersRoutes from './../../views/users/routes';

/* eslint-disable */
const Root = ({ store }) => (
      <AppContainer>
          <I18nextProvider i18n={i18n}>
              <Provider store={store}>
                  <Router history={browserHistory}>
                      <Redirect from="/" to="home" />
                      <Route path="/" component={App}>
                          <Route path="signin" component={Login} />
                          <Route path="forgot" component={ForgotPassword} />
                          {HomeRoutes}
                          {UsersRoutes}
                      </Route>
                  </Router>
              </Provider>
          </I18nextProvider>
      </AppContainer>
      );

Root.propTypes = {
  store: PropTypes.object.isRequired
};

export default Root;
