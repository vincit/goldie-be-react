package fi.vincit.sql;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * SQFileContent is a Querydsl query type for SQFileContent
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class SQFileContent extends com.querydsl.sql.RelationalPathBase<SQFileContent> {

    private static final long serialVersionUID = 1051790959;

    public static final SQFileContent fileContent = new SQFileContent("file_content");

    public final SimplePath<byte[]> fileData = createSimple("fileData", byte[].class);

    public final SimplePath<Object> fileMetadataUuid = createSimple("fileMetadataUuid", Object.class);

    public final com.querydsl.sql.PrimaryKey<SQFileContent> fileContentPkey = createPrimaryKey(fileMetadataUuid);

    public final com.querydsl.sql.ForeignKey<SQFileMetadata> fileContentMetadataUuidFk = createForeignKey(fileMetadataUuid, "file_metadata_uuid");

    public SQFileContent(String variable) {
        super(SQFileContent.class, forVariable(variable), "public", "file_content");
        addMetadata();
    }

    public SQFileContent(String variable, String schema, String table) {
        super(SQFileContent.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public SQFileContent(Path<? extends SQFileContent> path) {
        super(path.getType(), path.getMetadata(), "public", "file_content");
        addMetadata();
    }

    public SQFileContent(PathMetadata metadata) {
        super(SQFileContent.class, metadata, "public", "file_content");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(fileData, ColumnMetadata.named("file_data").withIndex(2).ofType(Types.BINARY).withSize(2147483647).notNull());
        addMetadata(fileMetadataUuid, ColumnMetadata.named("file_metadata_uuid").withIndex(1).ofType(Types.OTHER).withSize(2147483647).notNull());
    }

}

