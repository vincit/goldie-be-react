package fi.vincit.security.authorization.support;

import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.feature.account.user.SystemUserDTO;
import fi.vincit.feature.common.BaseEntity;
import fi.vincit.security.authorization.api.EntityAuthorizationTarget;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Persistable;

import static org.assertj.core.api.Assertions.assertThat;

public class AuthorizationTargetFactoryTest {
    private AuthorizationTargetFactoryImpl factory = new AuthorizationTargetFactoryImpl();

    @Test
    public void testForNullReference() {
        assertThat(factory.create(null)).as("Should not return target").isNull();
    }

    @Test
    public void testCreateForString() {
        assertThat(factory.create("user:1234")).as("Should support String argument").isNotNull();
        assertThat(factory.create("user:")).as("Should support String argument without key").isNotNull();
        assertThat(factory.create("user")).as("Should support String argument without delimiter").isNotNull();
    }

    @Test
    public void testCreateForEntity() {
        assertThat(factory.create(new SystemUser())).as("Should support entity").isNotNull();
    }

    @Test
    public void testCreateForDTO() {
        assertThat(factory.create(new SystemUserDTO())).as("Should support DTO").isNotNull();
    }

    @Test
    public void testToString() {
        EntityAuthorizationTarget target = factory.create("user:1234");
        assertThat(target.toString()).as("Wrong message").isEqualTo("AuthorizationTargetImpl{type=user, id=1234, class=null}");
    }

    @Test
    public void testCreateForGenericObject() {
        assertThat(factory.create(new GeneralClassWithIdProperty())).as("Should support any object").isNotNull();
        assertThat(factory.create(new Object())).as("Should support any object").isNotNull();
        assertThat(factory.create(new Double(1))).as("Should support any object").isNotNull();
    }

    @Test
    public void testCreateForGenericClass() {
        assertThat(factory.create(Object.class)).as("Should support any class").isNotNull();
        assertThat(factory.create(SystemUser.class)).as("Should support any class").isNotNull();
    }

    @Test
    public void testWithInterface() {
        final Double object = Math.PI;
        EntityAuthorizationTarget mock = Mockito.mock(EntityAuthorizationTarget.class);

        Mockito.when(mock.getAuthorizationTarget(Mockito.any(Class.class))).thenReturn(object);
        Mockito.when(mock.getAuthorizationTargetClass()).thenReturn(Double.class);
        Mockito.when(mock.getAuthorizationTargetId()).thenReturn(123L);
        Mockito.when(mock.getAuthorizationTargetName()).thenReturn("mockDouble");

        EntityAuthorizationTarget target = factory.create(mock);

        assertThat(target.getAuthorizationTargetName()).as("Wrong type").isEqualTo("mockDouble");
        assertThat(target.getAuthorizationTargetId()).as("Wrong id").isEqualTo(123L);
        assertThat(target.getAuthorizationTargetClass()).as("Should have correct class").isEqualTo(Double.class);
        assertThat(target.getAuthorizationTarget(Double.class)).isSameAs(object).as("Should have reference");
    }

    @Test
    public void testCreateForReference() {
        EntityAuthorizationTarget target = factory.create("randomEntity", 958L);

        assertThat(target.getAuthorizationTargetName()).as("Wrong type").isEqualTo("randomEntity");
        assertThat(target.getAuthorizationTargetId()).as("Wrong id").isEqualTo(958L);
        assertThat(target.getAuthorizationTargetClass()).as("Should have unknown class").isNull();
        assertThat(target.getAuthorizationTarget(String.class)).as("Should have no reference").isNull();
    }

    @Test
    public void testForPersistentEntity() {
        SystemUser user = new SystemUser();
        user.setId(512L);

        EntityAuthorizationTarget target = factory.forPersistentEntity(user);

        assertThat(target.getAuthorizationTargetName()).as("Wrong type").isEqualTo(SystemUser.class.getCanonicalName());
        assertThat(target.getAuthorizationTargetId()).as("Wrong id").isEqualTo(new Long(512));
        assertThat(target.getAuthorizationTargetClass()).as("Should have correct class").isEqualTo(SystemUser.class);
        assertThat(target.getAuthorizationTarget(SystemUser.class)).as("Should have reference").isSameAs(user);
    }

    @Test
    public void testForDTO() {
        SystemUserDTO userDTO = new SystemUserDTO();
        userDTO.setId(256L);

        EntityAuthorizationTarget target = factory.forDTO(userDTO);

        assertThat(target.getAuthorizationTargetName()).as("Wrong type").isEqualTo(SystemUserDTO.class.getCanonicalName());
        assertThat(target.getAuthorizationTargetId()).as("Wrong id").isEqualTo(new Long(256));
        assertThat(target.getAuthorizationTargetClass()).as("Should have correct class").isEqualTo(SystemUserDTO.class);
        assertThat(target.getAuthorizationTarget(SystemUserDTO.class) == userDTO).as("Should have reference").isTrue();
    }

    @Test
    public void testGetReferenceWithImplementedInterface() {
        EntityAuthorizationTarget target = factory.create(new SystemUser());

        assertThat(target.getAuthorizationTarget(BaseEntity.class)).as("Should give reference").isNotNull();
        assertThat(target.getAuthorizationTarget(Persistable.class)).as("Should give reference").isNotNull();
        assertThat(target.getAuthorizationTarget(Object.class)).as("Should give reference").isNotNull();
    }

    @Test
    public void testGetReferenceWithDifferentTargetClass() {
        EntityAuthorizationTarget target = factory.create(new SystemUser());

        assertThat(target.getAuthorizationTarget(Double.class)).as("Should not give reference").isNull();
    }

    @Test
    public void testDecodeFromString() {
        EntityAuthorizationTarget target = factory.decodeFromString("userEntity:31243");

        assertThat(target.getAuthorizationTargetName()).as("Wrong type").isEqualTo("userEntity");
        assertThat(target.getAuthorizationTargetId()).as("Wrong id").isEqualTo(31243L);
        assertThat(target.getAuthorizationTargetClass()).as("Should have unknown class").isNull();
        assertThat(target.getAuthorizationTarget(String.class)).as("Should have no reference").isNull();
    }

    @Test
    public void testDecodeFromStringWithoutPrimaryKey() {
        EntityAuthorizationTarget target = factory.decodeFromString("userEntity");

        assertThat(target.getAuthorizationTargetName()).as("Wrong type").isEqualTo("userEntity");
        assertThat(target.getAuthorizationTargetId()).as("Should not have id").isNull();
        assertThat(target.getAuthorizationTargetClass()).as("Should have unknown class").isNull();
        assertThat(target.getAuthorizationTarget(String.class)).as("Should have no reference").isNull();
    }

    @Test
    public void testDecodeFromStringWithoutTypeOrPrimaryKey() {
        assertThat(factory.decodeFromString("")).as("Should not return target").isNull();
        assertThat(factory.decodeFromString("   ")).as("Should not return target").isNull();
        assertThat(factory.decodeFromString(":")).as("Should not return target").isNull();
    }

    @Test
    public void testCreateForGeneralObject() {
        final Double object = Math.PI;
        EntityAuthorizationTarget target = factory.forObject(object);

        assertThat(target.getAuthorizationTargetName()).as("Wrong type").isEqualTo("java.lang.Double");
        assertThat(target.getAuthorizationTargetId()).as("Should not have id").isNull();
        assertThat(target.getAuthorizationTargetClass()).as("Should have correct class").isEqualTo(Double.class);
        assertThat(target.getAuthorizationTarget(Double.class)).as("Should have reference").isSameAs(object);
    }

    @Test
    public void testCreateForGeneralClass() {
        AuthorizationTargetImpl<Class<SystemUser>> target = factory.forClass(SystemUser.class);

        assertThat(target.getAuthorizationTargetName()).as("Wrong type").isEqualTo("SystemUser");
        assertThat(target.getAuthorizationTargetId()).as("Should not have id").isNull();
        assertThat(target.getAuthorizationTargetClass()).as("Should have correct class").isEqualTo(SystemUser.class);
        assertThat(target.getAuthorizationTarget(SystemUser.class)).as("Should have no reference").isNull();
    }

    @Test
    public void testCreateForGeneralObjectWithIdProperty() {
        final GeneralClassWithIdProperty object = new GeneralClassWithIdProperty();

        EntityAuthorizationTarget target = factory.forObject(object);

        assertThat(target.getAuthorizationTargetName()).as("Wrong type")
                .isEqualTo(this.getClass().getCanonicalName() + ".GeneralClassWithIdProperty");
        assertThat(target.getAuthorizationTargetId()).as("Wrong id").isEqualTo(256L);
        assertThat(target.getAuthorizationTargetClass()).as("Should have correct class").isEqualTo(GeneralClassWithIdProperty.class);
        assertThat(target.getAuthorizationTarget(GeneralClassWithIdProperty.class)).as("Should have reference").isSameAs(object);
    }

    public static class GeneralClassWithIdProperty {
        public Long getId() {
            return 256L;
        }
    }
}
