package fi.vincit.security.authorization.support;

import fi.vincit.security.authorization.api.EntityAuthorizationTarget;
import fi.vincit.security.authorization.spi.EntityAuthorizationStrategy;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class EntityAuthorizationStrategyRegistryTest {

    @Mock
    private EntityAuthorizationStrategy authorizationStrategy;

    @Mock
    private EntityAuthorizationTarget target;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    private EntityAuthorizationStrategyRegistry create() {
        EntityAuthorizationStrategyRegistry registry = new EntityAuthorizationStrategyRegistry();

        Mockito.when(authorizationStrategy.getEntityName()).thenReturn("java.lang.Integer");
        Mockito.when(authorizationStrategy.getSupportedTypes()).thenReturn(new Class[]{Double.class});

        ReflectionTestUtils.setField(registry, "strategies", Collections.singletonList(authorizationStrategy));

        registry.configure();

        return registry;
    }

    @Test
    public void testWithNullReference() {
        EntityAuthorizationStrategyRegistry registry = create();

        EntityAuthorizationStrategy strategy = registry.lookupAuthorizationStrategy(null);

        assertThat(strategy).isNotNull();
        assertThat(strategy.getEntityName()).isEqualTo("notImplemented");
        assertThat(strategy.getSupportedTypes()).isEqualTo(new Class[]{Void.class});
        assertThat(strategy.hasPermission(null, null, null)).isFalse();
    }

    @Test
    public void testWithInvalidTarget() {
        EntityAuthorizationStrategyRegistry registry = create();

        EntityAuthorizationStrategy strategy = registry.lookupAuthorizationStrategy(target);

        assertThat(strategy).isNotNull();
        assertThat(strategy.getEntityName()).isEqualTo("notImplemented");
    }

    @Test
    public void testWithEntityName() {
        Mockito.when(target.getAuthorizationTargetName()).thenReturn("java.lang.Integer");

        EntityAuthorizationStrategyRegistry registry = create();

        EntityAuthorizationStrategy strategy = registry.lookupAuthorizationStrategy(target);

        assertThat(strategy).isNotNull().isSameAs(authorizationStrategy);
    }

    @Test
    public void testWithCanonicalClassName() {
        Mockito.when(target.getAuthorizationTargetClass()).thenReturn(Integer.class);

        EntityAuthorizationStrategyRegistry registry = create();

        EntityAuthorizationStrategy strategy = registry.lookupAuthorizationStrategy(target);

        assertThat(strategy).isNotNull().isSameAs(authorizationStrategy);
    }

    @Test
    public void testWithEntityClass() {
        Mockito.when(target.getAuthorizationTargetClass()).thenReturn(Double.class);

        EntityAuthorizationStrategyRegistry registry = create();

        EntityAuthorizationStrategy strategy = registry.lookupAuthorizationStrategy(target);

        assertThat(strategy).isNotNull().isSameAs(authorizationStrategy);
    }

}
