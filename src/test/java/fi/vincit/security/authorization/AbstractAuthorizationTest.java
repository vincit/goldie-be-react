package fi.vincit.security.authorization;

import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.security.EntityPermission;
import fi.vincit.security.UserInfo;
import fi.vincit.security.authentication.TestAuthenticationTokenUtil;
import fi.vincit.security.authorization.api.EntityAuthorizationTarget;
import fi.vincit.security.authorization.spi.EntityAuthorizationStrategy;
import fi.vincit.security.authorization.support.AuthorizationTokenCollector;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;

public class AbstractAuthorizationTest {
    public enum TestEntityRole {
        ROLE_ENTITY;
    }

    static class SimpleAuthorizationStrategy extends AbstractEntityAuthorization {
        protected SimpleAuthorizationStrategy() {
            super("simpleEntity");

            allow(EntityPermission.READ, SystemUser.Role.ROLE_USER);
            allow("write", SystemUser.Role.ROLE_ADMIN);
            allow("other", TestEntityRole.ROLE_ENTITY);
        }

        @Override
        public Class[] getSupportedTypes() {
            return new Class[]{Void.class};
        }

        @Override
        protected void authorizeTarget(final AuthorizationTokenCollector collector,
                                       final EntityAuthorizationTarget target,
                                       final UserInfo userInfo) {
            collector.addAuthorizationRole(TestEntityRole.ROLE_ENTITY);
        }
    }

    @Mock
    private RoleHierarchy roleHierarchy;

    @Mock
    private EntityAuthorizationTarget target;

    private PasswordEncoder passwordEncoder = NoOpPasswordEncoder.getInstance();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    private EntityAuthorizationStrategy create() {
        SimpleAuthorizationStrategy strategy = new SimpleAuthorizationStrategy();

        Mockito.when(roleHierarchy.getReachableGrantedAuthorities(Mockito.anyCollection())).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) {
                return invocationOnMock.getArguments()[0];
            }
        });

        ReflectionTestUtils.setField(strategy, "roleHierarchy", roleHierarchy);

        return strategy;
    }

    private Authentication createAuthenticationWithoutAuthorities() {
        return TestAuthenticationTokenUtil.createAuthentication(
                "testUser", "testPassword", 1L, SystemUser.Role.ROLE_USER, false, passwordEncoder);
    }

    private Authentication createAuthentication(final SystemUser.Role role) {
        return TestAuthenticationTokenUtil.createAuthentication(
                "testUser", "testPassword", 1L, role, true, passwordEncoder);
    }

    @Test
    public void testSimple() {
        EntityAuthorizationStrategy strategy = create();

        assertThat(strategy.getEntityName()).isEqualTo("simpleEntity");
        assertThat(strategy.getSupportedTypes()).isEqualTo(new Class[]{Void.class});
    }

    @Test
    public void testPermission() {
        EntityAuthorizationStrategy strategy = create();

        assertThat(strategy.hasPermission(target, "read", createAuthenticationWithoutAuthorities())).isFalse();
        assertThat(strategy.hasPermission(target, "write", createAuthenticationWithoutAuthorities())).isFalse();
        assertThat(strategy.hasPermission(target, "write", createAuthentication(SystemUser.Role.ROLE_USER))).isFalse();
        assertThat(strategy.hasPermission(target, "delete", createAuthentication(SystemUser.Role.ROLE_ADMIN))).isFalse();

        assertThat(strategy.hasPermission(target, "read", createAuthentication(SystemUser.Role.ROLE_USER))).isTrue();
        assertThat(strategy.hasPermission(target, "write", createAuthentication(SystemUser.Role.ROLE_ADMIN))).isTrue();

        assertThat(strategy.hasPermission(target, "other", createAuthenticationWithoutAuthorities())).isFalse();
        assertThat(strategy.hasPermission(target, "other", createAuthentication(SystemUser.Role.ROLE_USER))).isTrue();
    }

    @Test
    public void testUnknownPermission() {
        EntityAuthorizationStrategy strategy = create();

        assertThat(strategy.hasPermission(target, "unknown", createAuthenticationWithoutAuthorities())).isFalse();
        assertThat(strategy.hasPermission(target, "unknown", createAuthentication(SystemUser.Role.ROLE_USER))).isFalse();
        assertThat(strategy.hasPermission(target, "unknown", createAuthentication(SystemUser.Role.ROLE_ADMIN))).isFalse();
    }

    @Test
    public void testNonStringPermission() {
        EntityAuthorizationStrategy strategy = create();

        assertThat(strategy.hasPermission(target, new Object(), createAuthentication(SystemUser.Role.ROLE_USER))).isFalse();
        assertThat(strategy.hasPermission(target, EntityPermission.READ, createAuthentication(SystemUser.Role.ROLE_USER))).isTrue();
    }

    @Test
    public void testRoleHierarchyApplied() {
        EntityAuthorizationStrategy strategy = create();

        Authentication authentication = createAuthentication(SystemUser.Role.ROLE_USER);

        // Invoke
        boolean hasPermission = strategy.hasPermission(target, "read", authentication);
        assertThat(hasPermission).isTrue();

        // Verify invocation
        Mockito.verify(roleHierarchy, Mockito.times(1)).getReachableGrantedAuthorities(
                Mockito.same(authentication.getAuthorities()));
    }
}
