package fi.vincit;

import fi.vincit.feature.account.user.SystemUser;
import org.junit.Test;

import javax.persistence.PersistenceException;

public class DatabaseSmokeTest extends EmbeddedDatabaseTest {

    @Test
    @HibernateStatisticsAssertions(maxQueries = 1)
    public void testPersistUser() {
        createNewUser("test", SystemUser.Role.ROLE_USER);

        persistInNewTransaction();
    }

    @Test(expected = PersistenceException.class)
    @HibernateStatisticsAssertions(maxQueries = 2)
    public void testPersistDuplicateUsername() {
        createNewUser("test", SystemUser.Role.ROLE_USER);
        createNewUser("test", SystemUser.Role.ROLE_USER);

        persistInNewTransaction();
    }

}
