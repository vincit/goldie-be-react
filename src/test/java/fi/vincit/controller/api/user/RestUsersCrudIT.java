package fi.vincit.controller.api.user;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.specification.ResponseSpecification;
import fi.vincit.controller.api.AbstractRestApiIT;
import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.feature.account.user.SystemUserDTO;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

public class RestUsersCrudIT extends AbstractRestApiIT {

    private static ResponseSpecification isValidUser() {
        return new ResponseSpecBuilder()
                .expectBody("id", Matchers.notNullValue(Long.class))
                .expectBody("rev", Matchers.notNullValue())
                .expectBody("active", Matchers.notNullValue())
                .expectBody("username", Matchers.not(Matchers.isEmptyOrNullString()))
                .expectBody("role", Matchers.notNullValue())
                .expectBody("password", Matchers.nullValue())
                .expectBody("passwordConfirm", Matchers.nullValue())
                .build();
    }

    private static ResponseSpecification isUpdatedUser(final SystemUserDTO dto) {
        final ResponseSpecBuilder builder = new ResponseSpecBuilder();

        if (dto.getId() != null) {
            builder.expectBody("id", Matchers.equalTo(dto.getId().intValue()));
        } else {
            builder.expectBody("id", Matchers.notNullValue(Long.class));
        }

        // If role is not specified, then original value should remain
        if (dto.getRole() != null) {
            builder.expectBody("role", Matchers.hasToString(dto.getRole().toString()));
        } else {
            builder.expectBody("role", Matchers.equalTo(SystemUser.Role.ROLE_USER));
        }

        if (dto.getLocale() != null) {
            builder.expectBody("locale", Matchers.equalTo(dto.getLocale().toString()));
        }

        if (dto.getTimeZone() != null) {
            builder.expectBody("timeZone", Matchers.equalTo(dto.getTimeZone().getID()));
        }

        return builder
                .expectBody("active", Matchers.equalTo(dto.isActive()))
                .expectBody("username", Matchers.equalTo(dto.getUsername()))
                .expectBody("firstName", Matchers.equalTo(dto.getFirstName()))
                .expectBody("lastName", Matchers.equalTo(dto.getLastName()))
                // Never expose current password value
                .expectBody("password", Matchers.nullValue())
                .expectBody("passwordConfirm", Matchers.nullValue())
                .expectBody("email", Matchers.equalTo(dto.getEmail()))
                .build();
    }

    @Test
    public void testNotFound() {
        givenAdminAuthentication()
                .expect()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .contentType(ContentType.JSON)
                .body("message", Matchers.equalTo("No such fi.vincit.feature.account.user.SystemUser id=999999"))
                .body("status", Matchers.equalTo("ERROR"))
                .when()
                .get(Constants.API_PREFIX + "/999999");
    }

    @Test
    public void testInvalidPrimaryKey() {
        givenAdminAuthentication()
                .expect()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .contentType(ContentType.JSON)
                .body("message", Matchers.equalTo("Bad Request"))
                .body("status", Matchers.equalTo("ERROR"))
                .when()
                .get(Constants.API_PREFIX + "/1234221231231312312312");
    }

    @Test
    public void testIllegalPrimaryKey() {
        // TODO: Should return JSON
        givenAdminAuthentication()
                .expect()
                .statusCode(HttpStatus.NOT_FOUND.value())
                //.contentType(ContentType.JSON)
                .when()
                .get(Constants.API_PREFIX + "/abcdefg");
    }

    @Test
    public void testFound() {
        givenAdminAuthentication()
                .expect()
                .statusCode(HttpStatus.OK.value())
                .when()
                .get(Constants.API_PREFIX + "/1");
    }

    @Test
    public void testContent() {
        givenAdminAuthentication()
                .expect()
                .statusCode(HttpStatus.OK.value())
                .contentType(ContentType.JSON)
                .specification(isValidUser())
                .body("id", Matchers.equalTo(1))
                .when()
                .get(Constants.API_PREFIX + "/1");
    }

    @Test
    public void testCreate() {
        final SystemUserDTO dto = new SystemUserDTO();

        dto.setUsername(UUID.randomUUID().toString());
        dto.setFirstName("firstname");
        dto.setLastName("lastname");
        dto.setEmail("user@example.org");
        dto.setLocale(Locale.CANADA);
        dto.setTimeZone(TimeZone.getDefault());
        dto.setPassword("password");
        dto.setPasswordConfirm("password");
        dto.setActive(true);
        dto.setRole(SystemUser.Role.ROLE_USER);

        givenAdminAuthentication()
                .contentType(ContentType.JSON)
                .body(dto)
                .expect()
                .statusCode(HttpStatus.CREATED.value())
                .contentType(ContentType.JSON)
                .spec(isValidUser())
                .spec(isUpdatedUser(dto))
                .when()
                .post(Constants.API_PREFIX);
    }

    @Test
    public void testUpdate() {
        final SystemUserDTO dto = new SystemUserDTO();
        dto.setUsername(UUID.randomUUID().toString());
        dto.setRole(SystemUser.Role.ROLE_USER);
        dto.setPassword("password");
        dto.setPasswordConfirm("password");

        final long userId = givenAdminAuthentication()
                .contentType(ContentType.JSON)
                .body(dto)
                .expect().statusCode(HttpStatus.CREATED.value())
                .contentType(ContentType.JSON)
                .when()
                .post(Constants.API_PREFIX).jsonPath().getLong("id");

        dto.setId(userId);
        dto.setActive(false);
        dto.setFirstName("firstname-modify");
        dto.setLastName("lastname-modify");
        dto.setEmail("user-modify@example.org");

        givenAdminAuthentication()
                .contentType(ContentType.JSON)
                .body(dto)
                .expect().statusCode(HttpStatus.OK.value())
                .contentType(ContentType.JSON)
                .spec(isValidUser())
                .spec(isUpdatedUser(dto))
                .when().put(Constants.API_PREFIX + "/" + userId);
    }

    @Test
    public void testDelete() {
        final SystemUserDTO dto = new SystemUserDTO();
        dto.setUsername(UUID.randomUUID().toString());
        dto.setRole(SystemUser.Role.ROLE_USER);
        dto.setActive(true);
        dto.setPassword("password");
        dto.setPasswordConfirm("password");

        final long userId = givenAdminAuthentication()
                .contentType(ContentType.JSON)
                .body(dto)
                .expect().statusCode(HttpStatus.CREATED.value())
                .contentType(ContentType.JSON)
                .when()
                .post(Constants.API_PREFIX).jsonPath().getLong("id");

        givenAdminAuthentication()
                .contentType(ContentType.JSON)
                .expect().statusCode(HttpStatus.NO_CONTENT.value())
                .when()
                .delete(Constants.API_PREFIX + "/" + userId);
    }
}
