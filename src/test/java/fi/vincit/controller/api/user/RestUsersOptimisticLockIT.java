package fi.vincit.controller.api.user;

import com.jayway.restassured.http.ContentType;
import fi.vincit.controller.api.AbstractRestApiIT;
import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.feature.account.user.SystemUserDTO;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class RestUsersOptimisticLockIT extends AbstractRestApiIT {
    @Test
    public void testUpdateConflict() {
        // Original copy
        final SystemUserDTO dto = new SystemUserDTO();
        dto.setUsername(UUID.randomUUID().toString());
        dto.setRole(SystemUser.Role.ROLE_USER);
        dto.setPassword("password");
        dto.setPasswordConfirm("password");
        // Rev number update from client should always be ignored by server!
        dto.setRev(new Integer(10));

        // Create
        final SystemUserDTO first = givenAdminAuthentication()
                .contentType(ContentType.JSON)
                .body(dto)
                .expect().statusCode(HttpStatus.CREATED.value())
                .contentType(ContentType.JSON)
                .when().post(Constants.API_PREFIX).as(SystemUserDTO.class);
        Assert.assertNotNull("No initial Rev", first.getRev());
        Assert.assertEquals("Rev should start from 0", new Integer(0), first.getRev());

        // Update #1 - no conflict
        dto.setId(first.getId());
        dto.setRev(first.getRev());
        dto.setFirstName("firstname-change1");
        dto.setLastName("lastname-change1");
        dto.setEmail("user-change1@example.org");

        final SystemUserDTO second = givenAdminAuthentication()
                .contentType(ContentType.JSON)
                .body(dto)
                .expect().statusCode(HttpStatus.OK.value())
                .contentType(ContentType.JSON)
                .when().put(Constants.API_PREFIX + "/" + first.getId()).as(SystemUserDTO.class);
        Assert.assertNotNull(second.getRev());
        Assert.assertEquals("Version should increment by one\", new Integer(2)", new Integer(1 + first.getRev()), second.getRev());

        // Update #2 - conflict because old version number!
        dto.setFirstName("firstname-change2");
        dto.setLastName("lastname-change2");
        dto.setEmail("user-change2@example.org");

        givenAdminAuthentication()
                .contentType(ContentType.JSON)
                .body(dto)
                .expect().statusCode(HttpStatus.CONFLICT.value())
                .contentType(ContentType.JSON)
                .when().put(Constants.API_PREFIX + "/" + first.getId());
    }
}
