package fi.vincit.validation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class FinnishPersonalIdentityValidatorTest {

    private static LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();

    @BeforeClass
    public static void setUpBeforeClass() {
        validator.afterPropertiesSet();
    }

    public class TestIdentityNumber {
        @FinnishPersonalIdentity
        public final String personalIdentityNumber;

        public TestIdentityNumber(String personalIdentityNumber) {
            this.personalIdentityNumber = personalIdentityNumber;
        }
    }

    @Test
    public void nullPersonalIdentityNumberShouldBeValid() {
        TestIdentityNumber testIdentityNumber = new TestIdentityNumber(null);
        Set<ConstraintViolation<TestIdentityNumber>> constraintViolations = validator.validate(testIdentityNumber);
        assertThat(constraintViolations).isEmpty();
    }

    @Test
    public void tooShortPersonalIdentityNumberShouldNotBeValid() {
        TestIdentityNumber testIdentityNumber = new TestIdentityNumber("121212A000");
        Set<ConstraintViolation<TestIdentityNumber>> constraintViolations = validator.validate(testIdentityNumber);
        assertThat(constraintViolations).isNotEmpty();
    }

    @Test
    public void tooLongPersonalIdentityNumberShouldNotBeValid() {
        TestIdentityNumber testIdentityNumber = new TestIdentityNumber("121212A000HH");
        Set<ConstraintViolation<TestIdentityNumber>> constraintViolations = validator.validate(testIdentityNumber);
        assertThat(constraintViolations).isNotEmpty();
    }

    @Test
    public void invalidBirthDatePersonalIdentityNumberShouldNotBeValid() {
        String[] invalidDates = {
                "300250-021J", "310650+033T", "310950A8375", "130950A837J", "310480-634A", "311181-735F",
                "121212+0150"
        };
        for (String personalIdentityNumber : invalidDates) {
            TestIdentityNumber testIdentityNumber = new TestIdentityNumber(personalIdentityNumber);
            Set<ConstraintViolation<TestIdentityNumber>> constraintViolations = validator.validate(testIdentityNumber);
            assertThat(constraintViolations).isNotEmpty();
        }
    }

    @Test
    public void personalIdentityNumber121212A000HShouldBeValid() {
        TestIdentityNumber testIdentityNumber = new TestIdentityNumber("121212A000H");
        Set<ConstraintViolation<TestIdentityNumber>> constraintViolations = validator.validate(testIdentityNumber);
        assertThat(constraintViolations).isEmpty();
    }

    @Test
    public void personalIdentityNumbersWithEveryAvailableCheckCharacterShouldBeValid() {
        String[] validPersonalIdentityNumbers = {
                "121212-0150", "121212-0161", "121212-0172", "121212-0183", "121212-0194", "121212-0205",
                "121212-0216", "121212-0227", "121212-0238", "121212-0559", "121212-025A", "121212A026B",
                "121212A027C", "121212-028D", "121212-029E", "121212A030F", "121212-031H", "121212-001J",
                "121212-002K", "121212A003L", "121212-004M", "121212-005N", "121212-006P", "121212-007R",
                "121212-008S", "121212-009T", "121212-010U", "121212-042V", "121212A012W", "121212-013X",
                "121212A014Y"
        };

        for (String validPersonalIdentityNumber : validPersonalIdentityNumbers) {
            TestIdentityNumber testIdentityNumber = new TestIdentityNumber(validPersonalIdentityNumber);
            Set<ConstraintViolation<TestIdentityNumber>> constraintViolations = validator.validate(testIdentityNumber);
            assertThat(constraintViolations)
                    .as(String.format("'%s' is is a valid personal identity number", validPersonalIdentityNumber))
                    .isEmpty();
        }
    }

    @Test
    public void personalIdentityNumberWithWrongCheckCharacterShouldNotBeValid() {
        TestIdentityNumber testIdentityNumber = new TestIdentityNumber("121212A000J");
        Set<ConstraintViolation<TestIdentityNumber>> constraintViolations = validator.validate(testIdentityNumber);
        assertThat(constraintViolations).isNotEmpty();
    }

    @Test
    public void personalIdentityNumberWithWrongDayShouldNotBeValid() {
        TestIdentityNumber testIdentityNumber = new TestIdentityNumber("131212A000H");
        Set<ConstraintViolation<TestIdentityNumber>> constraintViolations = validator.validate(testIdentityNumber);
        assertThat(constraintViolations).isNotEmpty();
    }

    @Test
    public void personalIdentityNumberWithWrongMonthShouldNotBeValid() {
        TestIdentityNumber testIdentityNumber = new TestIdentityNumber("121112A000H");
        Set<ConstraintViolation<TestIdentityNumber>> constraintViolations = validator.validate(testIdentityNumber);
        assertThat(constraintViolations).isNotEmpty();
    }

    @Test
    public void personalIdentityNumberWithWrongYearShouldNotBeValid() {
        TestIdentityNumber testIdentityNumber = new TestIdentityNumber("121213A000H");
        Set<ConstraintViolation<TestIdentityNumber>> constraintViolations = validator.validate(testIdentityNumber);
        assertThat(constraintViolations).isNotEmpty();
    }

    @Test
    public void personalIdentityNumberWithUnknownCenturyCharacterShouldNotBeValid() {
        TestIdentityNumber testIdentityNumber = new TestIdentityNumber("121212B000H");
        Set<ConstraintViolation<TestIdentityNumber>> constraintViolations = validator.validate(testIdentityNumber);
        assertThat(constraintViolations).isNotEmpty();
    }
}
