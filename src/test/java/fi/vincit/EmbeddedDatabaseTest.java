package fi.vincit;

import fi.vincit.config.Constants;
import fi.vincit.feature.account.ActiveUserService;
import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.test.DatabaseCleaner;
import fi.vincit.test.EntityPersister;
import fi.vincit.test.EntitySupplier;
import fi.vincit.test.TransactionalTaskExecutor;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Persistable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;

// This base test class being non-transactional is a distinct design decision.
@Rollback(false)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = EmbeddedDatabaseTestContext.class)
@ActiveProfiles(Constants.EMBEDDED_DATABASE)
public abstract class EmbeddedDatabaseTest {

    @Rule
    public HibernateStatisticsVerifier statsVerifier = new HibernateStatisticsVerifier() {
        @Override
        protected Statistics getStatistics() {
            return sessionFactory().getStatistics();
        }
    };

    @PersistenceContext
    private EntityManager entityManager;

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private ActiveUserService activeUserService;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private EntityPersister persister;

    @Resource
    private TransactionalTaskExecutor txExecutor;

    @Resource
    private DatabaseCleaner dbCleaner;

    private EntitySupplier entities;

    @PostConstruct
    public void initEntitySupplier() {
        entities = new EntitySupplier();
    }

    @Before
    public void initTest() {
        entities.clear();
        dbCleaner.clearManagedEntityTablesFromH2Database();
        clearHibernateStatistics();
    }

    protected SystemUser createNewUser(final String username, final SystemUser.Role role) {
        return entities.newUser(username, role, passwordEncoder);
    }

    protected void authenticate(final SystemUser user) {
        activeUserService.loginWithoutCheck(user);
    }

    protected void persistInNewTransaction() {
        persistInNewTransaction(entities.get());
        entities.clear();
    }

    protected void persistInNewTransaction(final Iterable<? extends Persistable<?>> entities) {
        persister.saveInNewTransaction(entities);
        entityManager.clear();
        clearHibernateStatistics();
    }

    protected void persistInCurrentlyOpenTransaction() {
        persistInCurrentlyOpenTransaction(entities.get());
        entities.clear();
    }

    protected void persistInCurrentlyOpenTransaction(final Iterable<? extends Persistable<?>> entities) {
        persister.saveInCurrentlyOpenTransaction(entities);
        sessionFactory().getStatistics().clear();
    }

    protected void runInTransaction(final Runnable runnable) {
        txExecutor.execute(runnable);
    }

    protected <T> T callInTransaction(final Callable<T> callable) {
        return txExecutor.execute(callable);
    }

    protected void assertStatisticsNowAndClear() {
        statsVerifier.verifyAndClear();
        entityManager.clear();
    }

    protected void clearHibernateStatistics() {
        sessionFactory().getStatistics().clear();
    }

    // Getters -->

    protected Random random() {
        return ThreadLocalRandom.current();
    }

    protected EntitySupplier model() {
        return entities;
    }

    protected ApplicationContext applicationContext() {
        return applicationContext;
    }

    protected EntityManager entityManager() {
        return entityManager;
    }

    protected SessionFactory sessionFactory() {
        return entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
    }
}
