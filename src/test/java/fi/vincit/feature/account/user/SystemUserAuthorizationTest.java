package fi.vincit.feature.account.user;

import fi.vincit.feature.account.AccountDTO;
import fi.vincit.security.audit.ConsoleAuthorizationAuditListener;
import fi.vincit.security.authentication.TestAuthenticationTokenUtil;
import fi.vincit.security.authorization.api.EntityAuthorizationTarget;
import fi.vincit.security.authorization.spi.AuthorizationAuditListener;
import fi.vincit.security.authorization.spi.EntityAuthorizationStrategy;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;

public class SystemUserAuthorizationTest {

    private AuthorizationAuditListener auditListener = new ConsoleAuthorizationAuditListener();

    @Mock
    private RoleHierarchy roleHierarchy;

    @Mock
    private EntityAuthorizationTarget target;

    private PasswordEncoder passwordEncoder = NoOpPasswordEncoder.getInstance();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    private EntityAuthorizationStrategy create() {
        SystemUserAuthorization strategy = new SystemUserAuthorization();

        Mockito.when(roleHierarchy.getReachableGrantedAuthorities(Matchers.anyCollectionOf(GrantedAuthority.class)))
                .thenAnswer((Answer<Object>) invocationOnMock -> invocationOnMock.getArguments()[0]);

        ReflectionTestUtils.setField(strategy, "roleHierarchy", roleHierarchy);

        return strategy;
    }

    private Authentication createAuthentication(String username, Long userId, SystemUser.Role role) {
        return TestAuthenticationTokenUtil.createAuthentication(username, "null", userId, role, true, passwordEncoder);
    }

    @Test
    public void testSimple() {
        EntityAuthorizationStrategy strategy = create();

        assertThat(strategy.getEntityName()).isEqualTo("SystemUser");
        assertThat(ArrayUtils.contains(strategy.getSupportedTypes(), SystemUser.class)).isTrue();
        assertThat(ArrayUtils.contains(strategy.getSupportedTypes(), SystemUserDTO.class)).isTrue();
        assertThat(ArrayUtils.contains(strategy.getSupportedTypes(), AccountDTO.class)).isTrue();
    }

    @Test
    public void testAuthorizeSelf() {
        EntityAuthorizationStrategy strategy = create();

        Mockito.when(target.getAuthorizationTargetClass()).thenReturn(SystemUser.class);
        Mockito.when(target.getAuthorizationTargetName()).thenReturn("user");
        Mockito.when(target.getAuthorizationTargetId()).thenReturn(1L);

        // Allowed read on self
        assertThat(strategy.hasPermission(target, "read", createAuthentication("user", 1L, SystemUser.Role.ROLE_USER))).isTrue();

        // Allow write, update on self
        assertThat(strategy.hasPermission(target, "update", createAuthentication("user", 1L, SystemUser.Role.ROLE_USER))).isTrue();
        assertThat(strategy.hasPermission(target, "changepassword", createAuthentication("user", 1L, SystemUser.Role.ROLE_USER))).isTrue();

        // Deny delete on self
        assertThat(strategy.hasPermission(target, "delete", createAuthentication("user", 1L, SystemUser.Role.ROLE_USER))).isFalse();

        // Deny create
        assertThat(strategy.hasPermission(target, "create", createAuthentication("user", 1L, SystemUser.Role.ROLE_USER))).isFalse();
    }

    @Test
    public void testDenyOperationOnOthersWhenNotAdmin() {
        EntityAuthorizationStrategy strategy = create();

        Mockito.when(target.getAuthorizationTargetClass()).thenReturn(SystemUser.class);
        Mockito.when(target.getAuthorizationTargetName()).thenReturn("user");
        Mockito.when(target.getAuthorizationTargetId()).thenReturn(1L);

        // Deny everything on others
        assertThat(strategy.hasPermission(target, "read", createAuthentication("user", 2L, SystemUser.Role.ROLE_USER))).isFalse();
        assertThat(strategy.hasPermission(target, "list", createAuthentication("user", 2L, SystemUser.Role.ROLE_USER))).isFalse();
        assertThat(strategy.hasPermission(target, "update", createAuthentication("user", 2L, SystemUser.Role.ROLE_USER))).isFalse();
        assertThat(strategy.hasPermission(target, "delete", createAuthentication("user", 2L, SystemUser.Role.ROLE_USER))).isFalse();
        assertThat(strategy.hasPermission(target, "create", createAuthentication("user", 2L, SystemUser.Role.ROLE_USER))).isFalse();
        assertThat(strategy.hasPermission(target, "changepassword", createAuthentication("user", 2L, SystemUser.Role.ROLE_USER))).isFalse();
    }

    @Test
    public void testAuthorizeAdmin() {
        EntityAuthorizationStrategy strategy = create();

        Mockito.when(target.getAuthorizationTargetClass()).thenReturn(SystemUser.class);
        Mockito.when(target.getAuthorizationTargetName()).thenReturn("user");
        Mockito.when(target.getAuthorizationTargetId()).thenReturn(1L);

        // Allow everything for admin on others
        assertThat(strategy.hasPermission(target, "read", createAuthentication("admin", 2L, SystemUser.Role.ROLE_ADMIN))).isTrue();
        assertThat(strategy.hasPermission(target, "list", createAuthentication("admin", 2L, SystemUser.Role.ROLE_ADMIN))).isTrue();
        assertThat(strategy.hasPermission(target, "update", createAuthentication("admin", 2L, SystemUser.Role.ROLE_ADMIN))).isTrue();
        assertThat(strategy.hasPermission(target, "create", createAuthentication("admin", 2L, SystemUser.Role.ROLE_ADMIN))).isTrue();
        assertThat(strategy.hasPermission(target, "delete", createAuthentication("admin", 2L, SystemUser.Role.ROLE_ADMIN))).isTrue();
        assertThat(strategy.hasPermission(target, "changepassword", createAuthentication("admin", 2L, SystemUser.Role.ROLE_ADMIN))).isTrue();
    }
}
