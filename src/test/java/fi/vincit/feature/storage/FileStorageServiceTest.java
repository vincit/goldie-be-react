package fi.vincit.feature.storage;

import com.google.common.hash.HashCode;
import fi.vincit.EmbeddedDatabaseTest;
import fi.vincit.feature.storage.metadata.FileType;
import fi.vincit.feature.storage.metadata.PersistentFileMetadata;
import org.junit.Test;
import org.springframework.http.MediaType;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.*;

public class FileStorageServiceTest extends EmbeddedDatabaseTest {

    @Resource
    private FileStorageService fileStorageService;

    @Test
    public void testDatabase() throws IOException {
        testBackend(FileType.TEST_DB);
    }

    @Test
    public void testLocalFolder() throws IOException {
        testBackend(FileType.TEST_FOLDER);
    }

    private void testBackend(final FileType fileType) throws IOException {
        final byte[] data = "Hello world".getBytes("UTF-8");

        final UUID uuid = UUID.randomUUID();

        fileStorageService.storeFile(uuid,
                data,
                fileType,
                MediaType.TEXT_PLAIN_VALUE,
                "test.txt");

        runInTransaction(() -> {
            final PersistentFileMetadata file = fileStorageService.getMetadata(uuid);

            assertThat(file.getStorageType()).isEqualTo(fileType.storageType());
            assertThat(file.getOriginalFileName()).isEqualTo("test.txt");
            assertThat(file.getContentType()).isEqualTo("text/plain");
            assertThat(file.getContentSize()).isEqualTo(11L);
            assertThat(file.getContentHash()).isEqualTo(HashCode.fromString("3e25960a79dbc69b674cd4ec67a72c62"));

            try {
                byte[] dbData = fileStorageService.getBytes(uuid);
                assertThat(dbData).containsExactly(data);
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        });
    }
}
