package fi.vincit.feature.mail;

import fi.vincit.feature.mail.delivery.MailDeliveryService;
import fi.vincit.feature.mail.queue.OutgoingMailProvider;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(MockitoJUnitRunner.class)
public abstract class BaseMailServiceTest {
    @Mock
    protected MailDeliveryService mailDeliveryService;

    @Mock
    protected OutgoingMailProvider outgoingMailProvider;

    @InjectMocks
    protected MailService mailService = new MailServiceImpl();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        ReflectionTestUtils.setField(mailService, "mailDeliveryEnabled", true);
        ReflectionTestUtils.setField(mailService, "fallbackDefaultEmailFromAddress", "root@example.org");
    }

    protected MailMessageDTO.Builder createTestMessageBuilder() {
        return new MailMessageDTO.Builder()
                //.withFrom("sender@example.org")
                .withTo("recipient@example.org")
                .withSubject("Email subject")
                .withBody("Hello world!");
    }
}
