/*
 * Copyright 2012-2016 Jarkko Kaura
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fi.vincit.conventions;

import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import fi.vincit.feature.common.BaseEntityDTO;
import fi.vincit.util.ClassInventory;
import fi.vincit.validation.DoNotValidate;
import fi.vincit.validation.FinnishBusinessId;
import fi.vincit.validation.FinnishPersonalIdentity;
import fi.vincit.validation.PhoneNumber;
import fi.vincit.validation.XssSafe;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.SafeHtml;
import org.junit.Test;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static fi.vincit.util.Asserts.assertEmpty;
import static fi.vincit.util.ReflectionTestUtils.fieldsOfClass;
import static fi.vincit.util.ReflectionTestUtils.methodsOfClass;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class DtoXssSafetyTest {

    @SuppressWarnings("unchecked")
    private static final Set<Class<? extends Annotation>> XSS_SAFE_MARKER_CLASSES = Sets.newHashSet(
            SafeHtml.class, Pattern.class, Email.class, FinnishBusinessId.class, FinnishPersonalIdentity.class,
            PhoneNumber.class, XssSafe.class);

    @SuppressWarnings("unchecked")
    private static final Set<Class<? extends Annotation>> VALID_ANNOTATION_CLASSES =
            Sets.newHashSet(Valid.class, Validated.class);

    private static final Predicate<Class<?>> IS_DTO_CLASS =
            clazz -> BaseEntityDTO.class.isAssignableFrom(clazz) || nameEndsWithDto(clazz);

    private static final Predicate<Method> IS_METHOD_HAVING_UNVALIDATED_DTO_PARAMETER = method -> {
        final Class<?>[] parameterTypes = method.getParameterTypes();
        final Annotation[][] annotations = method.getParameterAnnotations();

        for (int i = 0, numParameters = parameterTypes.length; i < numParameters; i++) {
            if (IS_DTO_CLASS.test(parameterTypes[i]) &&
                    !hasAnyAnnotation(annotations[i], VALID_ANNOTATION_CLASSES)) {
                return true;
            }
        }

        return false;
    };

    private static Stream<Method> getRequestMappingMethods() {
        return ClassInventory.getApiResourceClasses().stream()
                .flatMap(methodsOfClass(true))
                .filter(method -> method.isAnnotationPresent(RequestMapping.class))
                .filter(method -> Modifier.isPublic(method.getModifiers()));
    }

    private static Stream<Class<?>> getApiResourceParameterDtoClasses() {
        return getRequestMappingMethods().flatMap(DtoXssSafetyTest::getParameterDtoClasses);
    }

    private static Stream<Class<?>> getParameterDtoClasses(final Method method) {
        final Set<Class<?>> ret = Stream.of(method.getParameterTypes())
                .filter(IS_DTO_CLASS)
                .collect(toSet());

        ret.addAll(getContainedDtoClasses(ret));
        return ret.stream();
    }

    private static Set<Class<?>> getContainedDtoClasses(final Set<Class<?>> classes) {
        final Set<Class<?>> containedDtoClasses = classes.stream()
                .flatMap(fieldsOfClass(true))
                .map(Field::getType)
                .filter(clazz -> !classes.contains(clazz))
                .filter(IS_DTO_CLASS)
                .collect(toSet());

        // Dive one step deeper into class hierarchy.
        if (!containedDtoClasses.isEmpty()) {
            containedDtoClasses.addAll(getContainedDtoClasses(containedDtoClasses));
        }

        return containedDtoClasses;
    }

    private static boolean nameEndsWithDto(final Class<?> clazz) {
        final String className = clazz.getSimpleName();
        final int nameLen = className.length();
        return nameLen >= 3 && "DTO".equalsIgnoreCase(className.substring(nameLen - 3, nameLen));
    }

    private static boolean isOfStringTypeAndNotSafelyAnnotated(final Field field) {
        return String.class.equals(field.getType()) &&
                !hasAnyAnnotation(field.getAnnotations(), XSS_SAFE_MARKER_CLASSES);
    }

    private static final boolean hasAnyAnnotation(final Annotation[] fieldAnnotations,
                                                  final Set<Class<? extends Annotation>> annotations) {
        return getAnnotationClasses(fieldAnnotations).stream().anyMatch(a -> annotations.contains(a));
    }

    private static List<Class<? extends Annotation>> getAnnotationClasses(final Annotation[] annotations) {
        return Stream.of(annotations).map(Annotation::annotationType).collect(toList());
    }

    /**
     * Test that the DTO parameters of public methods of API resource classes
     * are annotated with @Valid/@Validated.
     */
    @Test
    public void testDtoParametersOfApiResourceMethodsAreValidated() {
        final Stream<Method> failingMethods = getRequestMappingMethods()
                .filter(IS_METHOD_HAVING_UNVALIDATED_DTO_PARAMETER)
                .sorted(Ordering.usingToString());

        assertEmpty(failingMethods,
                "The following methods should have their DTO parameters annotated with @Valid or @Validated: ");
    }

    /**
     * Test that the fields of DTO classes declared as input parameters of
     * public API methods are annotated properly to countermeasure XSS
     * injection.
     */
    @Test
    public void testApiResourceParameterDtoFieldsAreXssSafe() {
        final Stream<Field> failingFields = getApiResourceParameterDtoClasses()
                .flatMap(fieldsOfClass(true))
                .filter(DtoXssSafetyTest::isOfStringTypeAndNotSafelyAnnotated)
                .sorted(Ordering.usingToString());

        assertEmpty(failingFields,
                "The following DTO string fields should have XSS injection preventing annotation: ");
    }

    /**
     * Ascertain that the DTO fields of DTO classes declared as input parameters
     * of public API methods are annotated with @Valid in order to
     * countermeasure XSS injection.
     */
    @Test
    public void testDtoFieldsOfDtoClassParametersOfApiResourceMethodsAreValidated() {
        @SuppressWarnings("unchecked")
        final Set<Class<? extends Annotation>> expectedAnnotations = Sets.newHashSet(Valid.class, DoNotValidate.class);

        final Stream<Field> failingFields = getApiResourceParameterDtoClasses()
                .flatMap(fieldsOfClass(true))
                .filter(field -> IS_DTO_CLASS.test(field.getType()))
                .filter(field -> !hasAnyAnnotation(field.getAnnotations(), expectedAnnotations))
                .sorted(Ordering.usingToString());

        assertEmpty(failingFields,
                "The following DTO fields should be annotated with @Valid or @DoNotValidate because the DTO type " +
                        "appears as a field in another DTO class that is used as a parameter type of ApiResource method: ");
    }

}