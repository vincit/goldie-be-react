<%@ page import="org.hibernate.stat.Statistics,
                 org.hibernate.stat.EntityStatistics,
                 org.hibernate.stat.QueryStatistics,
                 org.hibernate.stat.SecondLevelCacheStatistics,
                 java.util.*" %>
<%@ page import="org.hibernate.jpa.HibernateEntityManagerFactory" %>
<%@ page import="org.hibernate.SessionFactory" %>
<%@ page import="org.springframework.web.servlet.support.RequestContextUtils" %>
<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="javax.persistence.EntityManagerFactory" %>

<html>
<body>
<%
    /**
     * This JSP will provide you tabular representation of Hibernate statistics.
     *
     * @src http://hibernatestats.blogspot.in/2007/02/page-import-org.html
     * @author Sandeep Dixit.
     */

    try {
        final WebApplicationContext webApplicationContext =
                RequestContextUtils.getWebApplicationContext(request);
        final EntityManagerFactory entityManagerFactory = webApplicationContext.getBean(EntityManagerFactory.class);
        final HibernateEntityManagerFactory hibernateEntityManagerFactory =
                (HibernateEntityManagerFactory) entityManagerFactory;
        final SessionFactory sessionFactory = hibernateEntityManagerFactory.getSessionFactory();

        Statistics statistics = sessionFactory.getStatistics();
%>
<center><h3>Hibernate Stats for server <<%=request.getServerName()%>> on {<%=new Date()%>}</h3></center>

<%
    if (!statistics.isStatisticsEnabled()) {
%>
<center><font color=red>
    <h3>hibernate.generate_statistics is set to false, and stats are not available.
        Only elements cache can be looked up.<br>
        to get the hibernate stats you need set
        hibernate.generate_statistics=true
        and restart the server.
    </h3></font></center>
<%
    }
%>
<table bgcolor="#dddddd" align=center width=60% border=2>
    <thead>
    <tr>
        <th colspan=2><B>General stats</B></th>
    </tr>
    </thead>
    <tr>
        <td>Connect count</td>
        <td><%=statistics.getConnectCount()%>
        </td>
    </tr>
    <tr>
        <td>SessionOpen Count</td>
        <td><%=statistics.getSessionOpenCount()%>
        </td>
    </tr>
    <tr>
        <td>SessionClose Count</td>
        <td><%=statistics.getSessionCloseCount()%>
        </td>
    </tr>
    <tr>
        <td>Flush Count</td>
        <td><%=statistics.getFlushCount()%>
        </td>
    </tr>

    <tr>
        <td>Entity Load Count</td>
        <td><%=statistics.getEntityLoadCount()%>
        </td>
    </tr>
    <tr>
        <td>Entity Fetch Count</td>
        <td><%=statistics.getEntityFetchCount()%>
        </td>
    </tr>
    <tr>
        <td>Entity Insert Count</td>
        <td><%=statistics.getEntityInsertCount()%>
        </td>
    </tr>
    <tr>
        <td>Entity Update Count</td>
        <td><%=statistics.getEntityUpdateCount()%>
        </td>
    </tr>

    <tr>
        <td>Collection Update Count</td>
        <td><%=statistics.getCollectionUpdateCount()%>
        </td>
    </tr>
    <tr>
        <td>Collection Remove Count</td>
        <td><%=statistics.getCollectionRemoveCount()%>
        </td>
    </tr>
    <tr>
        <td>Collection Load Count</td>
        <td><%=statistics.getCollectionLoadCount()%>
        </td>
    </tr>

    <tr>
        <td>Prepare Statement Count</td>
        <td><%=statistics.getPrepareStatementCount()%>
        </td>
    </tr>
    <tr>
        <td>CloseStatement Count</td>
        <td><%=statistics.getCloseStatementCount()%>
        </td>
    </tr>

    <tr>
        <td>QueryExecution Max Time</td>
        <td><%=statistics.getQueryExecutionMaxTime()%>
        </td>
    </tr>
    <tr>
        <td>Query CacheHit Count</td>
        <td><%=statistics.getQueryCacheHitCount()%>
        </td>
    </tr>
    <tr>
        <td>Query Cache Miss Count</td>
        <td><%=statistics.getQueryCacheMissCount()%>
        </td>
    </tr>
    <tr>
        <td>Query Cache Put Count</td>
        <td><%=statistics.getQueryCachePutCount()%>
        </td>
    </tr>

    <tr>
        <td>SecondLevelCache HitCount</td>
        <td><%=statistics.getSecondLevelCacheHitCount()%>
        </td>
    </tr>
    <tr>
        <td>SecondLevelCache MissCount</td>
        <td><%=statistics.getSecondLevelCacheMissCount()%>
        </td>
    </tr>
    <tr>
        <td>SecondLevelCache PutCount</td>
        <td><%=statistics.getSecondLevelCachePutCount()%>
        </td>
    </tr>

</table>
<br><br><br>
<table bgcolor="#dddd88" align=center width=60% border=2>
    <%
        try {
            String[] entityNames = statistics.getEntityNames();
    %>

    <thead>
    <tr>
        <th colspan=6><B>Entity stats [number of entities=<%=entityNames.length%>]</B></th>
    </tr>
    <%
        String strColumnHeaders =
                "<tr><th>class</th><th>Loads</th><th>fetch</th><th>Updates</th><th>Inserts</th><th>deletes</th></tr>";
    %>

    </thead>
    <%


        for (int entityIndex = 0; entityIndex < entityNames.length; entityIndex++) {
            EntityStatistics entityStatistics = statistics.getEntityStatistics(entityNames[entityIndex]);
    %>
    <%if (entityIndex % 20 == 0) {%> <%=strColumnHeaders%><%}%>
    <tr>
        <td><%=entityNames[entityIndex]%>
        </td>
        <td><%=entityStatistics.getLoadCount()%>
        </td>
        <td><%=entityStatistics.getFetchCount()%>
        </td>
        <td><%=entityStatistics.getUpdateCount()%>
        </td>
        <td><%=entityStatistics.getInsertCount()%>
        </td>
        <td><%=entityStatistics.getDeleteCount()%>
        </td>
    </tr>
    <%
            }
        } catch (Exception exp) {
            exp.printStackTrace(response.getWriter());
        }
    %>
</table>


<br><br><br>
<table bgcolor="#bbffff" align=center width=60% border=2>

    <thead>
    <tr>
        <th colspan=8><B>Query stats</B></th>
    </tr>
    <%
        {//code block
            String strColumnHeaders =

                    "<tr><th>Query</th>" +
                            "<th>Execution Count</th>" +
                            "<th>Execution AvgTime</th>" +
                            "<th>Max Time</th>" +
                            "<th>Min Time</th>" +
                            "<th>Execution Row</th>" +
                            "<th>Cache Hits</th>" +
                            "<th>Cache Puts</th>" +
                            "<th>Cache Miss</th>" +
                            "</tr>";
    %>

    </thead>
    <%
        class ComprableQueryStatsContainer implements Comparable {
            public String query;
            public QueryStatistics queryStatistics;

            public int compareTo(Object object) {
                if (!(object instanceof ComprableQueryStatsContainer)) {
                    throw new RuntimeException("Not instance of QueryComparator");
                }

                QueryStatistics queryStatisticsObject = ((ComprableQueryStatsContainer) object).queryStatistics;

                long diff = queryStatistics.getExecutionAvgTime() * queryStatistics.getExecutionCount();
                diff -= queryStatisticsObject.getExecutionAvgTime() * queryStatisticsObject.getExecutionCount();

                if (diff == 0) {
                    diff = queryStatistics.getExecutionMaxTime() - queryStatisticsObject.getExecutionMaxTime();
                }
                return (int) (-diff);
            }
        }

        TreeSet treeSet = new TreeSet();
        String[] queries = statistics.getQueries();
        for (int entityIndex = 0; entityIndex < queries.length; entityIndex++) {
            ComprableQueryStatsContainer comprableQueryStatsContainer = new ComprableQueryStatsContainer();
            QueryStatistics queryStatistics = statistics.getQueryStatistics(queries[entityIndex]);
            comprableQueryStatsContainer.queryStatistics = queryStatistics;
            comprableQueryStatsContainer.query = queries[entityIndex];
            treeSet.add(comprableQueryStatsContainer);
        }

        int index = 0;
        for (Iterator iterator = treeSet.iterator(); iterator.hasNext(); index++) {
            ComprableQueryStatsContainer comprableQueryStatsContainer = (ComprableQueryStatsContainer) iterator.next();
            QueryStatistics queryStatistics = comprableQueryStatsContainer.queryStatistics;
            String query = comprableQueryStatsContainer.query;

    %>

    <%if (index % 15 == 0) {%> <%=strColumnHeaders%><%}%>
    <tr>
        <td><%=query%>
        </td>
        <td><%=queryStatistics.getExecutionCount()%>
        </td>
        <td><%=queryStatistics.getExecutionAvgTime()%>
        </td>
        <td><%=queryStatistics.getExecutionMaxTime()%>
        </td>
        <td><%=queryStatistics.getExecutionMinTime()%>
        </td>
        <td><%=queryStatistics.getExecutionRowCount()%>
        </td>

        <td><%=queryStatistics.getCacheHitCount()%>
        </td>
        <td><%=queryStatistics.getCachePutCount()%>
        </td>
        <td><%=queryStatistics.getCacheMissCount()%>
        </td>

    </tr>
    <%
            }
        }
    %>
</table>


<br><br><br>
<table align=center bgcolor="#ddbbdd" width=60% border=2>

    <thead>
    <tr>
        <th colspan=7><B>Cache stats</B></th>
    </tr>
    <%
        {
            String strColumnHeaders =
                    "<tr><th>Cache Region</th>" +
                            "<th>Count In Memory</th>" +
                            "<th>Count On Disk</th>" +
                            "<th>Hits</th>" +
                            "<th>Puts</th>" +
                            "<th>Miss count</th>" +
                            "<th>catche Size</th>" +
                            "</tr>";
    %>

    </thead>
    <%

        String[] cacheRegionNames = statistics.getSecondLevelCacheRegionNames();
        for (int entityIndex = 0; entityIndex < cacheRegionNames.length; entityIndex++) {
            SecondLevelCacheStatistics cacheStatistics = statistics.getSecondLevelCacheStatistics(cacheRegionNames[entityIndex]);

    %>
    <%if (entityIndex % 20 == 0) {%> <%=strColumnHeaders%><%}%>
    <tr>
        <td><a href="#<%=cacheRegionNames[entityIndex]%>"><%=cacheRegionNames[entityIndex]%>
        </a></td>
        <td><%=cacheStatistics.getElementCountInMemory()%>
        </td>
        <td><%=cacheStatistics.getElementCountOnDisk()%>
        </td>

        <td><%=cacheStatistics.getHitCount()%>
        </td>
        <td><%=cacheStatistics.getPutCount()%>
        </td>
        <td><%=cacheStatistics.getMissCount()%>
        </td>
        <td><%=cacheStatistics.getSizeInMemory()%>
        </td>

    </tr>
    <%
            }
        }
    %>
</table>


<%
    {

        String[] cacheRegionNames = statistics.getSecondLevelCacheRegionNames();
        for (int entityIndex = 0; entityIndex < cacheRegionNames.length; entityIndex++) {

            SecondLevelCacheStatistics cacheStatistics = statistics.getSecondLevelCacheStatistics(cacheRegionNames[entityIndex]);
            Map entries = cacheStatistics.getEntries();
            Set keySet = entries.keySet();

%>
<br><br><br><a id="<%=cacheRegionNames[entityIndex]%>"></a>
<table bgcolor="#dddddd" align=center width=60% border=2>

    <tr>
        <td colspan=2>List of elements of <b><%=cacheRegionNames[entityIndex]%>
        </b> cache, number of elements=<%=entries.size()%>
        </td>
    </tr>
    <%
        Iterator keyIterator = keySet.iterator();
        while (keyIterator.hasNext()) {
            Object key = keyIterator.next();
            Object value = entries.get(key);
    %>
    <tr>
        <td><%=key%>
        </td>
        <td><%=value%>
        </td>
    </tr>
    <%
        }//while loop
    %>
</table>
<%

            }//for loop for cache regions

        }//code block

        if (request.getParameter("reset") != null) {
            statistics.clear();
        }

    } catch (Exception e) {
        e.printStackTrace(response.getWriter());
    }
%>


</body>
</html>
