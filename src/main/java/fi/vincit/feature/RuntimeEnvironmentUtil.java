package fi.vincit.feature;

import fi.vincit.config.Constants;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.net.URI;
import java.util.Locale;

@Component
public class RuntimeEnvironmentUtil {
    private final Environment environment;

    @Inject
    public RuntimeEnvironmentUtil(Environment environment) {
        this.environment = environment;
    }

    public Locale getDefaultLocale() {
        return new Locale("fi", "FI");
    }

    private String getEnvironmentId() {
        return environment.getRequiredProperty("environment.id");
    }

    public boolean isDevelopmentEnvironment() {
        return "dev".equalsIgnoreCase(getEnvironmentId());
    }

    public boolean isStagingEnvironment() {
        return "staging".equals(getEnvironmentId());
    }

    public boolean isProductionEnvironment() {
        return "prod".equalsIgnoreCase(getEnvironmentId());
    }

    public boolean isIntegrationTestEnvironment() {
        return isDevelopmentEnvironment() && environment.acceptsProfiles(Constants.EMBEDDED_DATABASE);
    }

    public boolean isEmailSendingEnabled() {
        return environment.getRequiredProperty("mail.enabled", boolean.class);
    }

    public String getFileStorageBasePath() {
        return environment.getProperty("file.storage.folder");
    }
}
