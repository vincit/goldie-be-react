package fi.vincit.feature;

import com.google.common.base.Preconditions;
import fi.vincit.feature.common.exception.NotFoundException;
import fi.vincit.feature.account.ActiveUserService;
import fi.vincit.feature.common.BaseEntity;
import fi.vincit.feature.common.BaseEntityDTO;
import fi.vincit.feature.common.ListTransformer;
import fi.vincit.security.EntityPermission;
import fi.vincit.util.DtoUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @param <ID> Entity's id class, often Long.class
 * @param <E>  Entity's class
 * @param <D>  DTO class corresponding to the entity
 */
public abstract class AbstractCrudFeature<ID extends Serializable,
        E extends BaseEntity<ID>,
        D extends BaseEntityDTO<ID>> {

    public enum TransformationLevel {
        FULL,
        BRIEF
    }

    @Resource
    protected ActiveUserService activeUserService;

    private final Class<? extends E> entityClass;

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected AbstractCrudFeature() {
        this.entityClass =
                (Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    protected abstract JpaRepository<E, ID> getRepository();

    protected abstract void updateEntity(E entity, D dto);

    protected abstract ListTransformer<E, D> dtoTransformer(final TransformationLevel level);

    @Transactional(readOnly = true)
    public Page<D> list(Pageable pageRequest) {
        // Permission check
        activeUserService.assertHasPermission(entityClass, EntityPermission.LIST);

        return toDTO(getRepository().findAll(pageRequest), pageRequest, TransformationLevel.BRIEF);
    }

    @Transactional(readOnly = true)
    public D read(ID id) {
        return toDTO(requireEntity(id, EntityPermission.READ), TransformationLevel.FULL);
    }

    @Transactional
    public D create(D dto) {
        final E entity = createEntity();

        updateEntity(entity, dto);

        // Permission check
        activeUserService.assertHasPermission(entity, EntityPermission.CREATE);

        return toDTO(getRepository().saveAndFlush(entity), TransformationLevel.FULL);
    }

    private E createEntity() {
        try {
            return entityClass.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException("Could not create entity", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Could access constructor", e);
        }
    }

    @Transactional
    public D update(D dto) {
        final E entity = requireEntity(dto.getId(), EntityPermission.UPDATE);

        checkForUpdateConflict(dto, entity);

        updateEntity(entity, dto);

        // Must use saveAndFlush() to update returned consistencyVersion == dto.revision
        return toDTO(getRepository().saveAndFlush(entity), TransformationLevel.FULL);
    }

    @Transactional
    public void delete(ID id) {
        delete(requireEntity(id, EntityPermission.DELETE));
    }

    protected void delete(E entity) {
        getRepository().delete(entity);
    }

    protected E requireEntity(final ID id, EntityPermission permission) {
        Preconditions.checkNotNull(id, "Entity primary key is required");

        final E entity = getRepository().findOne(id);

        if (entity == null) {
            throw new NotFoundException("No such " + entityClass.getCanonicalName() + " id=" + id);
        }

        // Permission check
        activeUserService.assertHasPermission(entity, permission);

        return entity;
    }

    protected void checkForUpdateConflict(D dto, E entity) {
        DtoUtil.assertNoVersionConflict(entity, dto);
    }

    // Single entity
    protected D toDTO(E entity, final TransformationLevel level) {
        return dtoTransformer(level).apply(entity);
    }

    // Page of entities
    protected Page<D> toDTO(final Page<E> resultPage, final Pageable pageRequest, final TransformationLevel level) {
        Objects.requireNonNull(resultPage, "resultPage must not be null");
        Objects.requireNonNull(pageRequest, "pageRequest must not be null");

        final List<D> transformedList = dtoTransformer(level).apply(resultPage.getContent());
        final List<D> resultList = transformedList != null ? transformedList : Collections.emptyList();

        return new PageImpl<>(resultList, pageRequest, resultPage.getTotalElements());
    }
}
