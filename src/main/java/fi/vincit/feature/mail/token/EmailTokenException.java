package fi.vincit.feature.mail.token;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EmailTokenException extends RuntimeException {
    public EmailTokenException(final String errorMessage) {
        super(errorMessage);
    }
}