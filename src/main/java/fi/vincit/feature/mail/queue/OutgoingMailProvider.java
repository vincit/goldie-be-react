package fi.vincit.feature.mail.queue;

import fi.vincit.feature.mail.MailMessageDTO;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public interface OutgoingMailProvider {
    /**
     * Store messages for delivery.
     *
     * @param messageDTO    contains email headers and body.
     * @param scheduledTime is an optional time for the first scheduled delivery attempt.
     */
    void scheduleForDelivery(MailMessageDTO messageDTO, Optional<Instant> scheduledTime);

    /**
     * Find outgoing message which are:
     *
     * a) not marked as delivered
     * b) are scheduled to be sent before current time (not too early)
     * c) have not failed delivery (too many delivery attempts)
     */
    Map<Long, MailMessageDTO> getOutgoingBatch();

    /**
     * Store delivery status for a batch of messages.
     *
     * @param successful List of primary keys for successfully delivered messages.
     * @param failed     List of primary keys for messages which have failed the delivery attempt.
     */
    void storeDeliveryStatus(Set<Long> successful, Set<Long> failed);
}
