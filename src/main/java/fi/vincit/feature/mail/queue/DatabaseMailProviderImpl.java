package fi.vincit.feature.mail.queue;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import fi.vincit.feature.mail.MailMessageDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class DatabaseMailProviderImpl implements OutgoingMailProvider {

    @Resource
    MailMessageRepository mailMessageRepository;

    // How many times to retry after mail delivery error?
    @Value("${mail.batch.maximum.failures}")
    int maxSendFailures;

    // How many mail messages are sent in one batch operation?
    @Value("${mail.batch.size}")
    int batchSize;

    @Override
    @Transactional
    public void scheduleForDelivery(final MailMessageDTO messageDTO,
                                    final Optional<Instant> scheduledTime) {

        final PersistentMailMessage mailMessage = new PersistentMailMessage();

        mailMessage.setBody(messageDTO.getBody());
        mailMessage.setSubject(messageDTO.getSubject());
        mailMessage.setFromEmail(messageDTO.getFrom());
        mailMessage.setToEmail(messageDTO.getTo());

        mailMessage.setSubmitTime(Instant.now());
        mailMessage.setScheduledTime(scheduledTime.orElse(Instant.now()));
        mailMessage.setDeliveryTime(null);

        mailMessage.setFailureCounter(0);

        mailMessageRepository.save(mailMessage);
    }

    @Override
    @Transactional(readOnly = true)
    public Map<Long, MailMessageDTO> getOutgoingBatch() {

        final List<PersistentMailMessage> unsentMessages = mailMessageRepository.findUnsentMessages(
                maxSendFailures, Instant.now(), new PageRequest(0, batchSize));

        final ImmutableMap<Long, PersistentMailMessage> messageById =
                Maps.uniqueIndex(unsentMessages, PersistentMailMessage::getId);

        return Maps.transformValues(messageById, (mailMessage) -> {
            return new MailMessageDTO.Builder()
                    .withFrom(mailMessage.getFromEmail())
                    .withTo(mailMessage.getToEmail())
                    .withSubject(mailMessage.getSubject())
                    .withBody(mailMessage.getBody())
                    .build();
        });
    }

    @Override
    @Transactional
    public void storeDeliveryStatus(final Set<Long> successful, final Set<Long> failed) {
        for (final Long primaryKey : successful) {
            requireMailMessage(primaryKey).markAsDelivered();
        }

        for (final Long primaryKey : failed) {
            requireMailMessage(primaryKey).incrementFailureCounter();
        }
    }

    private PersistentMailMessage requireMailMessage(final Long primaryKey) {
        final PersistentMailMessage mailMessage = mailMessageRepository.findOne(primaryKey);

        if (mailMessage == null) {
            throw new IllegalArgumentException("No mailMessage with id=" + primaryKey);
        }

        return mailMessage;
    }
}
