package fi.vincit.feature.mail.delivery;

import com.google.common.base.Throwables;
import fi.vincit.config.Constants;
import fi.vincit.feature.mail.MailMessageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailParseException;
import org.springframework.mail.MailPreparationException;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class JavaMailDeliveryServiceImpl implements MailDeliveryService {
    private static final Logger LOG = LoggerFactory.getLogger(JavaMailDeliveryServiceImpl.class);

    private String protocol;
    private String host;
    private int port = -1;
    private String username;
    private String password;
    private Properties javaMailProperties = new Properties();

    private Session session;

    public synchronized Session getSession() {
        if (this.session == null) {
            this.session = Session.getInstance(this.javaMailProperties);
        }
        return this.session;
    }

    @Override
    public void send(final MailMessageDTO message) {
        final Transport transport = createTransport();

        try {
            doSend(transport, message);

        } catch (MessagingException ex) {
            throw Throwables.propagate(ex);

        } finally {
            destroyTransport(transport);
        }
    }

    @Override
    public void sendAll(
            final Map<Long, MailMessageDTO> outgoingBatch,
            final Set<Long> successfulMessages,
            final Set<Long> failedMessages) {

        final Transport transport = createTransport();

        try {
            for (final Map.Entry<Long, MailMessageDTO> entry : outgoingBatch.entrySet()) {
                final Long messageId = entry.getKey();
                final MailMessageDTO message = entry.getValue();

                try {
                    doSend(transport, message);
                    successfulMessages.add(messageId);
                } catch (MessagingException ex) {
                    failedMessages.add(messageId);
                }
            }
        } finally {
            destroyTransport(transport);
        }
    }

    private Transport createTransport() {
        final Transport transport;

        try {
            transport = getSession().getTransport(this.protocol);
            transport.connect(this.host, this.port, this.username, this.password);

        } catch (MessagingException ex) {
            throw Throwables.propagate(ex);
        }

        return transport;
    }

    private void destroyTransport(Transport transport) {
        try {
            if (transport != null) {
                transport.close();
            }

        } catch (MessagingException ex) {
            LOG.error("Failed to close server connection after message sending", ex);
        }
    }

    private void doSend(Transport transport, MailMessageDTO message) throws MessagingException {
        LOG.info("Attempting delivery using for outgoing email: {}", message);

        final MimeMessage mimeMessage = new MimeMessage(getSession());
        prepareMessage(mimeMessage, message);
        mimeMessage.saveChanges();
        transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
    }

    private void prepareMessage(MimeMessage mimeMessage, MailMessageDTO message) {
        try {
            final MimeMessageHelper messageHelper = new MimeMessageHelper(
                    mimeMessage, Constants.DEFAULT_CHARACTER_ENCODING);

            messageHelper.setTo(message.getTo());
            messageHelper.setFrom(message.getFrom());
            messageHelper.setSubject(message.getSubject());
            messageHelper.setText(message.getBody(), true);
            messageHelper.setValidateAddresses(true);

            mimeMessage.setSentDate(new Date());

        } catch (MessagingException ex) {
            throw new MailParseException(ex);

        } catch (Exception ex) {
            throw new MailPreparationException(ex);
        }
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public void setJavaMailProperties(Properties javaMailProperties) {
        this.javaMailProperties = javaMailProperties;
    }
}
