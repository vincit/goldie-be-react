package fi.vincit.feature.mail;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.cache.HighConcurrencyTemplateCache;
import com.github.jknack.handlebars.helper.DefaultHelperRegistry;
import com.github.jknack.handlebars.io.TemplateLoader;
import com.github.jknack.handlebars.springmvc.MessageSourceHelper;
import com.github.jknack.handlebars.springmvc.SpringTemplateLoader;
import fi.vincit.feature.mail.delivery.JavaMailDeliveryServiceImpl;
import fi.vincit.feature.mail.delivery.MailDeliveryService;
import fi.vincit.feature.mail.queue.DatabaseMailProviderImpl;
import fi.vincit.feature.mail.queue.OutgoingMailProvider;
import org.springframework.beans.PropertyAccessorUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;

import javax.annotation.Resource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:configuration/mail.properties")
public class MailContext {

    @Resource
    private Environment env;

    @Bean
    public TemplateLoader emailTemplateLoader(ResourceLoader resourceLoader) {
        SpringTemplateLoader templateLoader = new SpringTemplateLoader(resourceLoader);
        templateLoader.setPrefix("classpath:templates");
        templateLoader.setSuffix(".hbs");

        return templateLoader;
    }

    @Bean
    public Handlebars emailHandlebars(TemplateLoader templateLoader, MessageSource messageSource) {
        return new Handlebars(templateLoader)
                .with(new DefaultHelperRegistry())
                .with(new HighConcurrencyTemplateCache())
                .registerHelper("message", new MessageSourceHelper(messageSource));
    }

    @Bean
    public MailDeliveryService mailDeliveryService() {
        final JavaMailDeliveryServiceImpl sender = new JavaMailDeliveryServiceImpl();

        sender.setJavaMailProperties(getMailProperties());
        sender.setHost(env.getRequiredProperty("mail.smtp.host"));
        sender.setPort(env.getRequiredProperty("mail.smtp.port", int.class));
        sender.setUsername(env.getRequiredProperty("mail.smtp.username"));
        sender.setPassword(env.getRequiredProperty("mail.smtp.password"));
        sender.setProtocol(env.getRequiredProperty("mail.smtp.protocol"));

        return sender;
    }

    @Bean
    public OutgoingMailProvider outgoingMailProvider() {
        return new DatabaseMailProviderImpl();
    }

    private static Properties getMailProperties() {
        final Properties properties = new Properties();
        properties.setProperty("mail.smtp.connectiontimeout", "1000");
        properties.setProperty("mail.smtp.timeout", "5000");
        return properties;
    }
}
