package fi.vincit.feature.common;

import java.io.Serializable;

public abstract class BaseEntityDTO<PK extends Serializable> {

    private Integer rev;

    public abstract PK getId();

    public abstract void setId(final PK id);

    // To prevent deserialization, annotate DTO class with @JsonIgnoreProperties(value = "rev")
    public Integer getRev() {
        return rev;
    }

    public void setRev(final Integer rev) {
        this.rev = rev;
    };

    @Override
    public String toString() {
        String idPart = this.getId() == null ? ":<transient object>" : (":<" + this.getId().toString() + ">");
        return this.getClass().getName() + idPart;
    }
}
