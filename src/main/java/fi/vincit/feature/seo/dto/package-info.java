/**
 * Sitemap XML schema definitions.
 */
@XmlSchema(namespace = "http://www.sitemaps.org/schemas/sitemap/0.9",
        xmlns = {
                @XmlNs(prefix = "", namespaceURI = "http://www.sitemaps.org/schemas/sitemap/0.9")
        },
        elementFormDefault = XmlNsForm.QUALIFIED)
package fi.vincit.feature.seo.dto;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;