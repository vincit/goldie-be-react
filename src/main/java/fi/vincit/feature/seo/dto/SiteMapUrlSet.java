package fi.vincit.feature.seo.dto;


import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * XML element for sitemap urlset
 * @see <a href="http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd</a>
 */
@XmlAccessorType(value = XmlAccessType.NONE)
@XmlRootElement(name = "urlset")
public class SiteMapUrlSet {
    @XmlElements({@XmlElement(name = "url", type = SiteMapUrl.class)})
    private List<SiteMapUrl> urls = new ArrayList<>();

    public SiteMapUrlSet() {}

    public List<SiteMapUrl> getUrls() {
        return urls;
    }

    public void setUrls(List<SiteMapUrl> urls) {
        this.urls = urls;
    }

    public void addUrl(SiteMapUrl url) {
        urls.add(url);
    }
}
