package fi.vincit.feature.seo.dto;

import org.joda.time.DateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * XML element for sitemap url
 * @see <a href="http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd</a>
 */
@XmlAccessorType(value = XmlAccessType.NONE)
@XmlRootElement(name = "url")
public class SiteMapUrl {
    private SiteMapUrl(Builder builder) {
        setLoc(builder.loc);
        setLastmod(builder.lastmod);
        setChangefreq(builder.changefreq);
        setPriority(builder.priority);
    }

    public enum ChangeFrequency {
        ALWAYS,
        HOURLY,
        DAILY,
        WEEKLY,
        MONTHLY,
        YEARLY,
        NEVER;

        public String getChangefreq() {
            return name().toLowerCase();
        }
    }

    @XmlElement
    private String loc;

    @XmlElement
    private String lastmod;

    @XmlElement
    private String changefreq;

    @XmlElement
    private String priority;

    public SiteMapUrl() {}

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getLastmod() {
        return lastmod;
    }

    public void setLastmod(String lastmod) {
        this.lastmod = lastmod;
    }

    public void setLastmod(DateTime lastmod) {
        if (lastmod == null) {
            this.lastmod = null;
        }
        else {
            this.lastmod = lastmod.toString();
        }
    }

    public String getChangefreq() {
        return changefreq;
    }

    public void setChangefreq(String changefreq) {
        if (changefreq == null) {
            this.changefreq = null;
            return;
        }

        for (ChangeFrequency frequency : ChangeFrequency.values()) {
            if (frequency.getChangefreq().equals(changefreq)) {
                this.changefreq = changefreq;
                return;
            }
        }

        throw new IllegalArgumentException("Invalid change frequency");
    }

    public void setChangefreq(ChangeFrequency changefreq) {
        if (changefreq == null) {
            this.changefreq = null;
        }
        else {
            this.changefreq = changefreq.getChangefreq();
        }
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        if (priority != null) {
            try {
                float prio = Float.parseFloat(priority);

                if (prio < 0 || prio > 1) {
                    throw new IllegalArgumentException("Priority must be between 0.0 and 1.0");
                }
            }
            catch (NumberFormatException e) {
                throw new IllegalArgumentException("Priority must be a number between 0.0 and 1.0");
            }
        }
        this.priority = priority;
    }

    public static final class Builder {
        private String loc;
        private String lastmod;
        private ChangeFrequency changefreq;
        private String priority;

        public Builder() {
        }

        public Builder withLoc(String val) {
            loc = val;
            return this;
        }

        public Builder withLastmod(String val) {
            lastmod = val;
            return this;
        }

        public Builder withChangefreq(ChangeFrequency val) {
            changefreq = val;
            return this;
        }

        public Builder withPriority(String val) {
            priority = val;
            return this;
        }

        public SiteMapUrl build() {
            return new SiteMapUrl(this);
        }
    }
}
