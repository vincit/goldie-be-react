package fi.vincit.feature.seo;

import fi.vincit.feature.common.exception.NotFoundException;
import fi.vincit.feature.seo.dto.SiteMapEntry;
import fi.vincit.feature.seo.dto.SiteMapIndex;
import fi.vincit.feature.seo.dto.SiteMapUrlSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Service for generating the sitemap index file and for delegating the
 * urlset file generation to the sitemap generator services {@link SitemapGeneratorService}
 *
 * @see SitemapGeneratorService
 */
@Service
public class SitemapService {
    @Autowired(required = false)
    private Map<String, SitemapGeneratorService> sitemapServices;

    /**
     * Generates the sitemap index file. The file will an entry for each of
     * the sitemap urlset generator service.
     * @return index file
     * @exception NotFoundException thrown if there are no sitemap generator services
     * @param request
     */
    public SiteMapIndex generateSitemapIndex(final HttpServletRequest request) throws NotFoundException {
        final UriComponentsBuilder uriBuilder = ServletUriComponentsBuilder.fromRequestUri(request);

        if (sitemapServices != null && !sitemapServices.isEmpty()) {
            SiteMapIndex index = new SiteMapIndex();

            for (Map.Entry<String, SitemapGeneratorService> entry : sitemapServices.entrySet()) {
                SiteMapEntry siteMap = new SiteMapEntry();
                siteMap.setLoc(uriBuilder.replacePath("sitemap_" + entry.getKey() + ".xml").toUriString());
                siteMap.setLastmod(entry.getValue().lastModified());
                index.addSitemap(siteMap);
            }

            return index;
        }
        else {
            throw new NotFoundException("Sitemap not defined");
        }
    }

    /**
     * Generates the sitemap urlset file by delegating the generation to a
     * {@link SitemapGeneratorService} instance.
     *
     * @param sitemapName the name of the sitemap service
     * @param request
     * @return the urlset file
     * @throws NotFoundException thrown if invalid service name is used
     */
    public SiteMapUrlSet generateDynamicSitemap(final String sitemapName,
                                                final HttpServletRequest request) throws NotFoundException {
        final SitemapGeneratorService service = sitemapServices.get(sitemapName);

        if (service == null) {
            throw new NotFoundException("Sitemap not found");
        }

        return service.generateSitemap(ServletUriComponentsBuilder.fromRequestUri(request));
    }
}
