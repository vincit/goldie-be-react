package fi.vincit.feature.account.user;

import fi.vincit.feature.account.AccountDTO;
import fi.vincit.security.UserInfo;
import fi.vincit.security.authorization.AbstractEntityAuthorization;
import fi.vincit.security.authorization.ConditionalAuthorization;
import fi.vincit.security.authorization.api.EntityAuthorizationTarget;
import fi.vincit.security.authorization.support.AuthorizationTokenCollector;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Authorization strategy to make decision on SystemUser domain entity.
 */
@Component
public class SystemUserAuthorization extends AbstractEntityAuthorization {
    public static final String CHANGE_PASSWORD = "changepassword";

    public enum Role {
        SELF
    }

    SystemUserAuthorization() {
        super("SystemUser");

        allow(READ, SystemUserAuthorization.Role.SELF);
        allow(UPDATE, SystemUserAuthorization.Role.SELF);
        allow(CHANGE_PASSWORD, SystemUserAuthorization.Role.SELF);

        allow(READ, SystemUser.Role.ROLE_ADMIN);
        allow(UPDATE, SystemUser.Role.ROLE_ADMIN);
        allow(CREATE, SystemUser.Role.ROLE_ADMIN);
        allow(DELETE, SystemUser.Role.ROLE_ADMIN);

        allow(LIST, SystemUser.Role.ROLE_ADMIN);
        allow(CHANGE_PASSWORD, SystemUser.Role.ROLE_ADMIN);
    }

    @Override
    protected void authorizeTarget(final AuthorizationTokenCollector collector,
                                   final EntityAuthorizationTarget target,
                                   final UserInfo userInfo) {
        collector.addAuthorizationRole(SystemUserAuthorization.Role.SELF, new ConditionalAuthorization() {
            @Override
            public boolean applies() {
                // Check if target entity is active user?
                return Objects.equals(target.getAuthorizationTargetId(), userInfo.getUserId());
            }
        });
    }

    @Override
    public Class[] getSupportedTypes() {
        return new Class[]{SystemUser.class, SystemUserDTO.class, AccountDTO.class};
    }
}
