package fi.vincit.feature.account.user;

import fi.vincit.feature.common.LifecycleEntity;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Locale;
import java.util.TimeZone;

@Entity
@Access(value = AccessType.FIELD)
@Where(clause = "deletion_time IS NULL")
public class SystemUser extends LifecycleEntity<Long> {
    public enum Role {
        ROLE_USER,
        ROLE_REST,
        ROLE_ADMIN;

        public String includes(Role that) {
            return this + " > " + that;
        }

        public boolean isAdmin() {
            return this == SystemUser.Role.ROLE_ADMIN;
        }
    }

    private Long id;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @NotBlank
    @Length(max = 255)
    @Column(length = 255, nullable = false, unique = true)
    private String username;

    @Length(max = 255)
    @Column(length = 255)
    private String password;

    @Column(name = "locale_id")
    private Locale locale;

    @Column(name = "timezone_id")
    private TimeZone timeZone;

    @Length(max = 255)
    @Column(nullable = true, length = 255)
    private String firstName;

    @Length(max = 255)
    @Column(nullable = true, length = 255)
    private String lastName;

    @Length(max = 255)
    @Column(nullable = true, length = 255)
    private String email;

    @Column(name = "is_active", nullable = false)
    private boolean active;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    @Column(name = "user_id", nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getHashedPassword() {
        return this.password;
    }

    public void setPasswordAsPlaintext(final String plainTextPassword,
                                       final PasswordEncoder passwordEncoder) {
        this.password = passwordEncoder.encode(plainTextPassword);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean isActive) {
        this.active = isActive;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
