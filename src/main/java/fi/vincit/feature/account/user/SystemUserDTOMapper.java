package fi.vincit.feature.account.user;

import com.google.common.collect.Lists;
import fi.vincit.feature.common.ListTransformer;
import fi.vincit.util.DtoUtil;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;

@Component
public class SystemUserDTOMapper extends ListTransformer<SystemUser, SystemUserDTO> {
    @Nonnull
    @Override
    protected List<SystemUserDTO> transform(@Nonnull final List<SystemUser> list) {
        return Lists.transform(list, user -> {
            final SystemUserDTO dto = new SystemUserDTO();
            toDTO(user, dto);
            return dto;
        });
    }

    public static void toDTO(final SystemUser user, final SystemUserDTO dto) {
        Objects.requireNonNull(user);
        Objects.requireNonNull(dto);

        DtoUtil.copyBaseFields(user, dto);

        dto.setUsername(user.getUsername());
        dto.setActive(user.isActive());
        dto.setRole(user.getRole());
        dto.setEmail(user.getEmail());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setLocale(user.getLocale());
        dto.setTimeZone(user.getTimeZone());
    }

    public static void toEntity(final SystemUser user, final SystemUserDTO dto) {
        Objects.requireNonNull(user);
        Objects.requireNonNull(dto);

        user.setUsername(dto.getUsername());
        user.setActive(dto.isActive());
        user.setRole(dto.getRole() != null ? dto.getRole() : SystemUser.Role.ROLE_USER);
        user.setEmail(dto.getEmail());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setLocale(dto.getLocale());
        user.setTimeZone(dto.getTimeZone());
    }
}
