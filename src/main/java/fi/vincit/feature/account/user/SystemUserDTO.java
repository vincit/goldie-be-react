package fi.vincit.feature.account.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fi.vincit.feature.common.BaseEntityDTO;
import fi.vincit.validation.XssSafe;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Locale;
import java.util.TimeZone;

public class SystemUserDTO extends BaseEntityDTO<Long> {
    // See user/form.html and account/password_reset.html.
    public static final int PASSWORD_MINIMUM_LENGTH = 6;

    public interface Create {
    }

    public interface Edit {
    }

    private Long id;

    private boolean active = true;

    @NotNull(groups = {Create.class})
    private SystemUser.Role role;

    @Size(min = 2, max = 63, groups = {Edit.class, Create.class})
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String username;

    @XssSafe
    @NotEmpty(groups = Create.class)
    @Size(min = PASSWORD_MINIMUM_LENGTH, groups = Create.class, message = "{javax.validation.constraints.Size.min.message}")
    private String password;

    @XssSafe
    @NotEmpty(groups = Create.class)
    private String passwordConfirm;

    @Email(groups = {Edit.class, Create.class})
    private String email;

    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, groups = {Edit.class, Create.class})
    private String firstName;

    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, groups = {Edit.class, Create.class})
    private String lastName;

    private TimeZone timeZone;

    private Locale locale;

    /*
    http://stackoverflow.com/questions/1972933/cross-field-validation-with-hibernate-validator-jsr-303
     */
    @AssertTrue(message = "password confirmation must match", groups = {Create.class, Edit.class})
    @JsonIgnore
    public boolean isPasswordConfirmMatch() {
        if (this.getId() == null) {
            // Password is only required when creating new user
            return this.password != null && this.password.equals(this.passwordConfirm);
        }
        return this.password == null || this.password.equals(this.passwordConfirm);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public SystemUser.Role getRole() {
        return role;
    }

    public void setRole(SystemUser.Role role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(final TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(final Locale locale) {
        this.locale = locale;
    }
}
