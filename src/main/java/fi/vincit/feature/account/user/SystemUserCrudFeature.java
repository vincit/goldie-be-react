package fi.vincit.feature.account.user;

import fi.vincit.feature.AbstractCrudFeature;
import fi.vincit.feature.account.password.change.ChangePasswordService;
import fi.vincit.feature.common.ListTransformer;
import fi.vincit.security.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.function.Function;

@Component
public class SystemUserCrudFeature extends AbstractCrudFeature<Long, SystemUser, SystemUserDTO> {

    @Resource
    private SystemUserRepository userRepository;

    @Resource
    private ChangePasswordService changePasswordService;

    @Resource
    private SystemUserDTOMapper userDTOMapper;

    @Override
    protected JpaRepository<SystemUser, Long> getRepository() {
        return userRepository;
    }

    @Override
    protected void updateEntity(SystemUser user, SystemUserDTO userDTO) {
        final UserInfo activeUserInfo = activeUserService.getActiveUserInfo();

        if (activeUserInfo.getRole().isAdmin()) {
            SystemUserDTOMapper.toEntity(user, userDTO);

            if (userDTO.getPassword() != null) {
                changePasswordService.setUserPassword(user, userDTO.getPassword());
            }
        }
    }

    @Override
    protected void delete(SystemUser user) {
        user.setActive(false);
        user.softDelete();
    }

    @Override
    protected ListTransformer<SystemUser, SystemUserDTO> dtoTransformer(final TransformationLevel level) {
        return userDTOMapper;
    }
}
