package fi.vincit.feature.account;

import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.feature.common.BaseEntityDTO;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.SafeHtml;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Locale;
import java.util.TimeZone;

public class AccountDTO extends BaseEntityDTO<Long> {

    private Long id;

    @NotNull
    @Size(min = 2, max = 63)
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String username;

    private SystemUser.Role role;

    @Email
    @Length(max = 255)
    private String email;

    @Length(max = 255)
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String firstName;

    @Length(max = 255)
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String lastName;

    private TimeZone timeZone;

    private Locale locale;

    AccountDTO() {
        super();
    }

    public AccountDTO(SystemUser user) {
        setId(user.getId());
        setRev(user.getConsistencyVersion());
        setUsername(user.getUsername());
        setRole(user.getRole());
        setEmail(user.getEmail());
        setFirstName(user.getFirstName());
        setLastName(user.getLastName());
        setTimeZone(user.getTimeZone());
        setLocale(user.getLocale());
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public final void setUsername(String username) {
        this.username = username;
    }

    public SystemUser.Role getRole() {
        return role;
    }

    public void setRole(SystemUser.Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(final TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(final Locale locale) {
        this.locale = locale;
    }
}
