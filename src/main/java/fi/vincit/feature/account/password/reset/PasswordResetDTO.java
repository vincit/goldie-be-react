package fi.vincit.feature.account.password.reset;

import fi.vincit.feature.account.user.SystemUserDTO;
import fi.vincit.validation.XssSafe;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

public class PasswordResetDTO {
    // Password reset token from email
    @XssSafe
    @NotBlank
    private String token;

    // New password
    @XssSafe
    @NotBlank
    @Size(min = SystemUserDTO.PASSWORD_MINIMUM_LENGTH, message = "{javax.validation.constraints.Size.min.message}")
    private String password;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
