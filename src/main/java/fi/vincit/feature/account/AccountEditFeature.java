package fi.vincit.feature.account;

import com.google.common.base.Preconditions;
import fi.vincit.feature.account.audit.AccountAuditService;
import fi.vincit.feature.account.password.change.ChangePasswordDTO;
import fi.vincit.feature.account.password.change.ChangePasswordService;
import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.security.EntityPermission;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

@Component
@PreAuthorize("hasRole('ROLE_USER')")
public class AccountEditFeature {
    @Resource
    private ActiveUserService activeUserService;

    @Resource
    private ChangePasswordService changePasswordService;

    @Resource
    private AccountAuditService accountAuditService;

    @Transactional(readOnly = true)
    public AccountDTO getActiveAccount() {
        final SystemUser activeUser = activeUserService.getActiveUser();

        activeUserService.assertHasPermission(activeUser, EntityPermission.READ);

        return new AccountDTO(activeUser);
    }

    @Transactional
    public void updateActiveAccount(AccountDTO account) {
        final SystemUser activeUser = activeUserService.getActiveUser();

        activeUserService.assertHasPermission(activeUser, EntityPermission.UPDATE);
        activeUser.setUsername(account.getUsername());
        activeUser.setFirstName(account.getFirstName());
        activeUser.setLastName(account.getLastName());
        activeUser.setEmail(account.getEmail());
        activeUser.setLocale(account.getLocale());
        activeUser.setTimeZone(account.getTimeZone());
    }

    @Transactional
    public void changeActiveUserPassword(ChangePasswordDTO dto) {
        Preconditions.checkArgument(StringUtils.hasText(dto.getPassword()), "No password given");

        final SystemUser user = activeUserService.getActiveUser();

        activeUserService.assertHasPermission(user, EntityPermission.UPDATE);

        changePasswordService.setUserPassword(user, dto.getPassword());

        // Log account activity
        accountAuditService.auditPasswordChange(user, activeUserService.getAuthentication());
    }
}
