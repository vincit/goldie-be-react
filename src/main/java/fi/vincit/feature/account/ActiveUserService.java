package fi.vincit.feature.account;

import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.feature.account.user.SystemUserRepository;
import fi.vincit.security.UserInfo;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class ActiveUserService {
    @Resource
    private SystemUserRepository userRepository;

    @Resource
    private PermissionEvaluator permissionEvaluator;

    private AuthenticationTrustResolver authTrustResolver = new AuthenticationTrustResolverImpl();

    public boolean isAuthenticatedUser() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        return authentication != null && authentication.isAuthenticated()
                && authentication.getPrincipal() instanceof UserInfo;
    }

    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY, noRollbackFor = RuntimeException.class)
    public SystemUser getActiveUser() {
        final SystemUser user = userRepository.findOne(getActiveUserId());

        if (user == null) {
            throw new IllegalStateException("User for authenticated principal does not exists in repository");
        }

        return user;
    }

    public Long getActiveUserId() {
        final UserInfo activeUserInfo = getActiveUserInfo();

        if (activeUserInfo != null && activeUserInfo.getUserId() != null) {
            return activeUserInfo.getUserId();
        }
        throw new RuntimeException("User id not available in security context");
    }

    public UserInfo getActiveUserInfo() {
        final SecurityContext context = SecurityContextHolder.getContext();

        if (context != null && isAuthenticated(context.getAuthentication())) {
            return UserInfo.extractFrom(context.getAuthentication());
        }

        return null;
    }

    private boolean isAuthenticated(Authentication authentication) {
        return authentication != null && authentication.getPrincipal() != null
                && !authTrustResolver.isAnonymous(authentication) && authentication.isAuthenticated();
    }

    public void assertHasPermission(final Object targetObject, final Object permission) {
        if (!checkHasPermission(targetObject, permission)) {
            throw new AccessDeniedException("PermissionEvaluator returned false");
        }
    }

    public boolean checkHasPermission(final Object targetObject, final Object permission) {
        final SecurityContext securityContext = SecurityContextHolder.getContext();

        if (securityContext == null) {
            throw new IllegalStateException("SecurityContext is not available");
        }

        final Authentication authentication = securityContext.getAuthentication();

        return permissionEvaluator.hasPermission(authentication, targetObject, permission);
    }

    public void loginWithoutCheck(final SystemUser systemUser) {
        final Authentication authentication = UserInfo.create(systemUser).createAuthentication();

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
