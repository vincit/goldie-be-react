package fi.vincit.feature.account.rememberme;

import fi.vincit.feature.account.user.SystemUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.LockModeType;

public interface PersistentLoginRepository extends JpaRepository<PersistentLogin, String> {
    @Query("SELECT p FROM PersistentLogin p WHERE p.id = ?1")
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    PersistentLogin findOneWithWriteLock(String series);

    @Modifying
    @Query("delete from PersistentLogin p where p.username = ?1")
    void deleteByUsername(String username);}
