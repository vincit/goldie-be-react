package fi.vincit.feature.account.rememberme;

import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.security.UserInfo;
import fi.vincit.security.authentication.RememberMeCookie;
import fi.vincit.security.crypto.Base64StringKeyGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.keygen.StringKeyGenerator;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.time.Instant;

@Service
@Transactional(noRollbackFor = AuthenticationException.class)
public class PersistentLoginService {
    private static final Logger LOG = LoggerFactory.getLogger(PersistentLoginService.class);

    @Resource
    private PersistentLoginRepository tokenRepository;

    @Resource
    private UserDetailsService userDetailsService;

    // Used to generate random remember-me token value and series
    private StringKeyGenerator pseudoRandomGenerator =
            Base64StringKeyGenerator.withByteLength(RememberMeCookie.RANDOM_BYTE_COUNT);

    @Transactional
    public RememberMeCookie create(Authentication successfulAuthentication, HttpServletRequest request) {
        final UserInfo userInfo = UserInfo.extractFrom(successfulAuthentication);

        if (userInfo.getRole() == SystemUser.Role.ROLE_REST) {
            return null;
        }

        final PersistentLogin token = new PersistentLogin();
        token.setUsername(userInfo.getUsername());
        token.setLastUsed(Instant.now());
        token.setRemoteAddress(request.getRemoteAddr());
        token.setUserAgent(request.getHeader("User-Agent"));

        final RememberMeCookie cookie = RememberMeCookie.create(pseudoRandomGenerator);
        token.setSeries(cookie.getSeries());

        return RememberMeCookie.forPersistentCookie(tokenRepository.save(token));
    }

    @Transactional
    public UserDetails validate(final RememberMeCookie presentedCookie,
                                final HttpServletRequest request) {
        // Use write-lock to prevent optimistic locking exception
        final PersistentLogin token = tokenRepository.findOneWithWriteLock(presentedCookie.getSeries());

        if (token == null) {
            // No series match, so we can't authenticate using this cookie
            throw new RememberMeAuthenticationException("No persistent token found for series id: "
                    + presentedCookie.getSeries());
        }

        if (token.isOlderThan(RememberMeCookie.TTL)) {
            throw new RememberMeAuthenticationException("Remember-me login has expired");
        }

        token.setRemoteAddress(request.getRemoteAddr());
        token.setUserAgent(request.getHeader("User-Agent"));
        token.setLastUsed(Instant.now());

        return userDetailsService.loadUserByUsername(token.getUsername());
    }

    @Transactional
    public void delete(final RememberMeCookie cookie) {
        final PersistentLogin token = tokenRepository.findOne(cookie.getSeries());

        if (token != null) {
            tokenRepository.delete(token);
        }
    }

    @Transactional
    public void deleteAll(final SystemUser systemUser) {
        tokenRepository.deleteByUsername(systemUser.getUsername());
    }
}
