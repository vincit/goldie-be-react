package fi.vincit.feature.account.rememberme;

import fi.vincit.feature.common.BaseEntity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Duration;
import java.time.Instant;

@Entity
@Access(value = AccessType.FIELD)
public class PersistentLogin extends BaseEntity<String> {

    private String series;

    @NotNull
    @Size(max=255)
    @Column(nullable = false)
    private String username;

    @NotNull
    @Column(nullable = false)
    private Instant lastUsed;

    @Column
    private String remoteAddress;

    @Column(columnDefinition = "TEXT")
    private String userAgent;

    @Override
    public String getId() {
        return series;
    }

    @Override
    public void setId(String id) {
        this.series = id;
    }

    @Id
    @Size(max=255)
    @Access(value = AccessType.PROPERTY)
    @Column(nullable = false)
    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Instant getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(Instant lastUsed) {
        this.lastUsed = lastUsed;
    }

    public boolean isOlderThan(final Duration validityPeriod) {
        return lastUsed == null || lastUsed.isBefore(Instant.now().minus(validityPeriod));
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
}
