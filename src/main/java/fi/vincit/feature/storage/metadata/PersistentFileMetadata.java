package fi.vincit.feature.storage.metadata;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import fi.vincit.feature.common.LifecycleEntity;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.UUID;

@Entity
@Access(value = AccessType.FIELD)
@Table(name = "file_metadata")
public class PersistentFileMetadata extends LifecycleEntity<UUID> {
    public static PersistentFileMetadata create(final UUID uuid,
                                                final StorageType storageType,
                                                final String contentType,
                                                final byte[] content,
                                                final String originalFilename) {
        final PersistentFileMetadata metadata = new PersistentFileMetadata();

        metadata.setId(Objects.requireNonNull(uuid));
        metadata.setStorageType(Objects.requireNonNull(storageType, "Storage type is null"));
        metadata.setContentType(Objects.requireNonNull(contentType));
        metadata.setContentSize(Objects.requireNonNull(content, "content is null").length);
        metadata.setContentHash(Hashing.md5().hashBytes(content));
        metadata.setOriginalFileName(originalFilename);

        return metadata;
    }

    public static PersistentFileMetadata create(final UUID uuid,
                                                final StorageType storageType,
                                                final MultipartFile multipartFile,
                                                final File tmpFile) throws IOException {
        final PersistentFileMetadata metadata = new PersistentFileMetadata();

        metadata.setId(Objects.requireNonNull(uuid));
        metadata.setStorageType(Objects.requireNonNull(storageType, "Storage type is null"));
        metadata.setContentType(Objects.requireNonNull(multipartFile.getContentType()));
        metadata.setContentSize(Objects.requireNonNull(multipartFile.getSize()));
        metadata.setContentHash(Files.hash(tmpFile, Hashing.md5()));
        metadata.setOriginalFileName(multipartFile.getOriginalFilename());

        return metadata;
    }

    private UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private StorageType storageType;

    @Column
    private String originalFileName;

    @NotBlank
    @Column(nullable = false)
    private String contentType;

    @Column(nullable = false)
    private long contentSize;

    @Column
    private String contentHash;

    // Private / public for example. S3-bucket URL address
    @Column
    private URL resourceUrl;

    // To make native UUID work on PG use JDBC parameter: stringtype=unspecified
    // https://github.com/pgjdbc/pgjdbc/issues/247
    @Override
    @Id
    @Type(type = "uuid-char")
    @Access(value = AccessType.PROPERTY)
    @Column(name = "file_metadata_uuid", columnDefinition = "UUID", nullable = false, updatable = false)
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(final StorageType storageType) {
        this.storageType = storageType;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public long getContentSize() {
        return contentSize;
    }

    public void setContentSize(long contentSize) {
        this.contentSize = contentSize;
    }

    public HashCode getContentHash() {
        return contentHash != null ? HashCode.fromString(this.contentHash) : null;
    }

    public void setContentHash(final HashCode value) {
        this.contentHash = value != null ? value.toString() : null;
    }

    public URL getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(URL resourceUrl) {
        this.resourceUrl = resourceUrl;
    }
}
