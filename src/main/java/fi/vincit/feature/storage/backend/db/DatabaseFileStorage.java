package fi.vincit.feature.storage.backend.db;

import com.google.common.base.Throwables;
import com.google.common.io.ByteStreams;
import fi.vincit.feature.storage.backend.FileStorageSpi;
import fi.vincit.feature.storage.metadata.FileType;
import fi.vincit.feature.storage.metadata.PersistentFileMetadata;
import fi.vincit.feature.storage.metadata.StorageType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Component
public class DatabaseFileStorage implements FileStorageSpi {
    private JdbcOperations jdbcTemplate;

    @Override
    public StorageType getType() {
        return StorageType.LOCAL_DATABASE;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    @Transactional(rollbackFor = IOException.class)
    public void retrieveFile(final PersistentFileMetadata metadata, final OutputStream outputStream) throws IOException {
        final Object[] params = {metadata.getId().toString()};

        jdbcTemplate.query("SELECT file_data FROM file_content WHERE file_metadata_uuid = ?", params, rs -> {
            try (final InputStream binaryStream = rs.getBinaryStream(1)) {
                ByteStreams.copy(binaryStream, outputStream);
            } catch (IOException e) {
                throw Throwables.propagate(e);
            }
        });
    }

    @Override
    @Transactional(rollbackFor = IOException.class)
    public void storeFile(final FileType fileType,
                          final PersistentFileMetadata metadata,
                          final InputStream inputStream) throws IOException {
        jdbcTemplate.update("INSERT INTO file_content (file_metadata_uuid, file_data) VALUES (?, ?)", ps -> {
            ps.setString(1, metadata.getId().toString());
            ps.setBinaryStream(2, inputStream);
        });
    }

    @Override
    public void removeFromStorage(final PersistentFileMetadata metadata) {
        jdbcTemplate.update("DELETE FROM file_content WHERE file_metadata_uuid = ?", metadata.getId().toString());
    }

    @Override
    public boolean isConfigured() {
        return true;
    }
}
