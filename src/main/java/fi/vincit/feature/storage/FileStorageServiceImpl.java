package fi.vincit.feature.storage;

import fi.vincit.feature.storage.backend.FileStorageSpi;
import fi.vincit.feature.storage.metadata.FileType;
import fi.vincit.feature.storage.metadata.PersistentFileMetadata;
import fi.vincit.feature.storage.metadata.PersistentFileMetadataRepository;
import fi.vincit.feature.storage.metadata.StorageType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.apache.commons.io.IOUtils;

@Service
@Transactional
public class FileStorageServiceImpl implements FileStorageService {
    @Resource
    private PersistentFileMetadataRepository metadataRepository;

    @Resource
    private List<FileStorageSpi> storages = Collections.emptyList();

    private FileStorageSpi getStorage(final StorageType storageType) {
        return storages.stream().filter(fss -> fss.getType() == storageType && fss.isConfigured())
                .findAny().orElseThrow(() -> new IllegalStateException("Storage is not configured: " + storageType));
    }

    private FileStorageSpi getStorageOrFallback(final StorageType storageType,
                                                final StorageType fallbackType) {
        return storages.stream().filter(fss -> fss.getType() == storageType && fss.isConfigured())
                .findAny().orElse(getStorage(fallbackType));
    }

    @Override
    @Transactional(readOnly = true)
    public boolean exists(final UUID uuid) {
        return metadataRepository.findOne(uuid) != null;
    }

    @Override
    @Transactional(readOnly = true)
    public PersistentFileMetadata getMetadata(final UUID uuid) {
        return metadataRepository.getOne(uuid);
    }

    @Override
    @Transactional(rollbackFor = IOException.class)
    public PersistentFileMetadata storeFile(final UUID uuid,
                                            final byte[] content,
                                            final FileType fileType,
                                            final String contentType,
                                            final String originalFilename) throws IOException {
        final FileStorageSpi storage = getStorageOrFallback(fileType.storageType(), StorageType.LOCAL_DATABASE);

        final PersistentFileMetadata fileMetadata = metadataRepository.saveAndFlush(
                PersistentFileMetadata.create(uuid, storage.getType(), contentType, content, originalFilename));

        storage.storeFile(fileType, fileMetadata, new ByteArrayInputStream(content));

        return fileMetadata;
    }

    @Override
    @Transactional(rollbackFor = IOException.class)
    public PersistentFileMetadata storeFile(final UUID uuid,
                                            final MultipartFile multipartFile,
                                            final FileType fileType) throws IOException {
        final FileStorageSpi storage = getStorageOrFallback(fileType.storageType(), StorageType.LOCAL_DATABASE);

        final File tmpFile = File.createTempFile("multipart", ".tmp");
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(tmpFile))) {
            IOUtils.copy(multipartFile.getInputStream(), bos);
        }

        try (final FileInputStream fis = new FileInputStream(tmpFile);
             final BufferedInputStream bis = new BufferedInputStream(fis)) {
            final PersistentFileMetadata fileMetadata = metadataRepository.saveAndFlush(
                    PersistentFileMetadata.create(uuid, storage.getType(), multipartFile, tmpFile));
            storage.storeFile(fileType, fileMetadata, bis);

            return fileMetadata;

        } finally {
            Files.deleteIfExists(tmpFile.toPath());
        }
    }

    @Override
    @Transactional(readOnly = true, rollbackFor = IOException.class)
    public byte[] getBytes(final UUID uuid) throws IOException {
        final PersistentFileMetadata metadata = metadataRepository.getOne(uuid);
        final FileStorageSpi fss = getStorage(metadata.getStorageType());

        try (final ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            fss.retrieveFile(metadata, bos);
            return bos.toByteArray();
        }
    }

    @Override
    @Transactional
    public void remove(final UUID uuid) {
        final PersistentFileMetadata metadata = metadataRepository.getOne(uuid);
        getStorage(metadata.getStorageType()).removeFromStorage(metadata);
        metadataRepository.delete(metadata);
    }
}
