package fi.vincit;

import fi.vincit.config.AopConfig;
import fi.vincit.config.AsyncConfig;
import fi.vincit.config.CacheConfig;
import fi.vincit.config.Constants;
import fi.vincit.config.DataSourceConfig;
import fi.vincit.config.JPAConfig;
import fi.vincit.config.LiquibaseConfig;
import fi.vincit.config.LocalizationConfig;
import fi.vincit.config.SecurityConfig;
import fi.vincit.config.postinit.PostInitialize;
import fi.vincit.feature.CreateInitialDatabaseContentFeature;
import fi.vincit.feature.mail.MailContext;
import fi.vincit.util.JCEUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Configuration
@Import({
        DataSourceConfig.class,
        LiquibaseConfig.class,
        JPAConfig.class,
        CacheConfig.class,
        LocalizationConfig.class,
        AsyncConfig.class,
        MailContext.class,
        SecurityConfig.class,
        AopConfig.class
})
@PropertySource({
        "classpath:configuration/application.properties",
        "classpath:git.properties"
})
@ComponentScan(Constants.FEATURE_BASE_PACKAGE)
public class GoldieApplicationContext {
    @Resource
    private CreateInitialDatabaseContentFeature initialDatabaseContentFeature;

    @PostInitialize
    public void afterStartup() {
        JCEUtil.removeJavaCryptographyAPIRestrictions();

        initialDatabaseContentFeature.initSampleContent();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    /**
     * Spring RestTemplate should be configured to use shared HTTP request factory.
     */
    @Bean
    public ClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(5 * 1000);
        factory.setReadTimeout(10 * 1000);

        return factory;
    }

    @Bean
    public RestTemplate defaultRestTemplate() {
        return new RestTemplate(clientHttpRequestFactory());
    }

    @Bean
    public boolean isRunningSpringBasedUnitTests() {
        return false;
    }
}
