package fi.vincit.security.audit;

import fi.vincit.feature.RuntimeEnvironmentUtil;
import fi.vincit.feature.account.audit.AccountAuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ClassUtils;

import javax.annotation.Resource;

@Component
public class AuthenticationAuditEventListener implements ApplicationListener<AbstractAuthenticationEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(AuthenticationAuditEventListener.class);

    @Resource
    private AccountAuditService accountAuditService;

    @Resource
    private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void onApplicationEvent(AbstractAuthenticationEvent event) {
        emitLogMessage(event);
        storeLogMessage(event);
    }

    private static void emitLogMessage(AbstractAuthenticationEvent event) {
        final StringBuilder builder = new StringBuilder();
        builder.append("Authentication event ");
        builder.append(ClassUtils.getShortName(event.getClass()));
        builder.append(": ");
        builder.append(event.getAuthentication().getName());
        builder.append("; details: ");
        builder.append(event.getAuthentication().getDetails());

        if (event instanceof AbstractAuthenticationFailureEvent) {
            builder.append("; exception: ");
            builder.append(((AbstractAuthenticationFailureEvent) event).getException().getMessage());
        }

        LOG.warn(builder.toString());
    }

    private void storeLogMessage(final AbstractAuthenticationEvent event) {
        try {
            if (event instanceof AuthenticationSuccessEvent) {
                accountAuditService.auditLoginSuccessEvent(AuthenticationSuccessEvent.class.cast(event));
            } else if (event instanceof AbstractAuthenticationFailureEvent) {
                accountAuditService.auditLoginFailureEvent(AbstractAuthenticationFailureEvent.class.cast(event));
            }
        } catch (Exception ex) {
            LOG.error("Failed to audit authentication event in database", ex);
        }
    }
}
