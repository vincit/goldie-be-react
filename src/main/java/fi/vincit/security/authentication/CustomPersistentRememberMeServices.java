package fi.vincit.security.authentication;

import fi.vincit.config.SecurityConfig;
import fi.vincit.feature.account.rememberme.PersistentLoginService;
import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class CustomPersistentRememberMeServices implements RememberMeServices, LogoutHandler {
    private static final Logger LOG = LoggerFactory.getLogger(CustomPersistentRememberMeServices.class);

    public static final String DEFAULT_PARAMETER = "remember_me";

    private final UserDetailsChecker userDetailsChecker = new AccountStatusUserDetailsChecker();
    private final AuthenticationDetailsSource<HttpServletRequest, ?>
            authenticationDetailsSource = new WebAuthenticationDetailsSource();

    @Resource
    private PersistentLoginService rememberMeTokenService;

    @Override
    public void loginSuccess(final HttpServletRequest request,
                             final HttpServletResponse response,
                             final Authentication successfulAuthentication) {
        if (BooleanUtils.toBoolean(request.getParameter(DEFAULT_PARAMETER))) {
            final RememberMeCookie rememberMeCookie = createCookie(request, successfulAuthentication);

            if (rememberMeCookie != null) {
                setCookie(request, response, rememberMeCookie);
                return;
            }
        }

        // Otherwise remove remember-me cookie if exists
        RememberMeCookie.cancelCookie(request, response);
    }

    @Override
    public void loginFail(final HttpServletRequest request, final HttpServletResponse response) {
        RememberMeCookie.cancelCookie(request, response);
    }

    @Override
    public Authentication autoLogin(final HttpServletRequest request, final HttpServletResponse response) {
        final RememberMeCookie presentedCookie = RememberMeCookie.forHttpRequest(request, response);

        if (presentedCookie == null) {
            return null;
        }

        try {
            final UserDetails userDetails = rememberMeTokenService.validate(presentedCookie, request);
            userDetailsChecker.check(userDetails);

            // Init session manually, because SessionCreationPolicy.NEVER does not create it by default
            request.getSession(true);

            return createSuccessfulAuthentication(request, userDetails);

        } catch (DataAccessException dae) {
            LOG.error("Failed to update token: ", dae);
            throw new RememberMeAuthenticationException("Autologin failed due to data access problem");
        } catch (AccountStatusException statusInvalid) {
            LOG.debug("Invalid UserDetails: " + statusInvalid.getMessage());
        } catch (RememberMeAuthenticationException e) {
            LOG.debug(e.getMessage());
        }

        cancelCookie(request, response, presentedCookie);

        return null;
    }

    @Override
    public void logout(final HttpServletRequest request, final HttpServletResponse response,
                       final Authentication authentication) {
        final RememberMeCookie presentedCookie = RememberMeCookie.forHttpRequest(request, response);

        if (presentedCookie != null) {
            cancelCookie(request, response, presentedCookie);
        }
    }

    protected Authentication createSuccessfulAuthentication(final HttpServletRequest request, final UserDetails user) {
        final RememberMeAuthenticationToken auth = new RememberMeAuthenticationToken(
                SecurityConfig.REMEMBER_ME_KEY, user, user.getAuthorities());
        auth.setDetails(authenticationDetailsSource.buildDetails(request));
        return auth;
    }

    private RememberMeCookie createCookie(final HttpServletRequest request,
                                          final Authentication successfulAuthentication) {
        try {
            return rememberMeTokenService.create(successfulAuthentication, request);

        } catch (DataAccessException e) {
            LOG.error("Failed to save persistent login token ", e);
        }

        return null;
    }

    private void setCookie(final HttpServletRequest request,
                           final HttpServletResponse response,
                           final RememberMeCookie rememberMeCookie) {
        response.addCookie(rememberMeCookie.asHttpCookie(request));
    }

    private void cancelCookie(final HttpServletRequest request,
                              final HttpServletResponse response,
                              final RememberMeCookie presentedCookie) {
        RememberMeCookie.cancelCookie(request, response);

        try {
            rememberMeTokenService.delete(presentedCookie);

        } catch (DataAccessException e) {
            LOG.error("Failed to remove persistent login token", e);
        }
    }
}
