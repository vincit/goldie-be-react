package fi.vincit.security.authentication;

import fi.vincit.feature.account.rememberme.PersistentLogin;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.crypto.keygen.StringKeyGenerator;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Duration;
import java.util.Objects;

public final class RememberMeCookie {
    public static final Duration TTL = Duration.ofDays(30);
    public static final String HTTP_COOKIE_NAME = "REMEMBER_ME";
    public static final int RANDOM_BYTE_COUNT = 16;

    public static RememberMeCookie create(final StringKeyGenerator keyGenerator) {
        return new RememberMeCookie(keyGenerator.generateKey());
    }

    /**
     * Sets a "cancel cookie" (with maxAge = 0) on the response to disable persistent logins.
     */
    public static void cancelCookie(final HttpServletRequest request, final HttpServletResponse response) {
        if (!isPresent(request)) {
            return;
        }

        final Cookie cookie = new Cookie(RememberMeCookie.HTTP_COOKIE_NAME, null);
        cookie.setMaxAge(0);
        cookie.setPath(getCookiePath(request));

        response.addCookie(cookie);
    }

    public static boolean isPresent(final HttpServletRequest request) {
        return findCookie(request) != null;
    }

    public static RememberMeCookie forPersistentCookie(final PersistentLogin token) {
        return new RememberMeCookie(token.getSeries());
    }

    public static RememberMeCookie forHttpRequest(final HttpServletRequest request,
                                                  final HttpServletResponse response) {
        try {
            return RememberMeCookie.forHttpCookie(findCookie(request));

        } catch (InvalidCookieException invalidCookie) {
            RememberMeCookie.cancelCookie(request, response);
        }

        return null;
    }

    private static RememberMeCookie forHttpCookie(final Cookie httpCookie) throws InvalidCookieException {
        if (httpCookie == null) {
            return null;
        }

        if (httpCookie.getValue() == null || httpCookie.getValue().isEmpty()) {
            throw new InvalidCookieException("Cookie content is empty");
        }

        final StringBuilder cookieValue = new StringBuilder(httpCookie.getValue());
        for (int j = 0; j < cookieValue.length() % 4; j++) {
            cookieValue.append('=');
        }

        final byte[] cookieBytes = cookieValue.toString().getBytes();
        if (!Base64.isBase64(cookieBytes)) {
            throw new InvalidCookieException("Cookie token was not Base64 encoded; value was '" + cookieValue + "'");
        }

        return new RememberMeCookie(new String(Base64.decode(cookieBytes)));
    }

    private static Cookie findCookie(HttpServletRequest request) {
        if (request.getCookies() != null) {
            for (final Cookie cookie : request.getCookies()) {
                if (RememberMeCookie.HTTP_COOKIE_NAME.equals(cookie.getName())) {
                    return cookie;
                }
            }
        }

        return null;
    }

    private static String getCookiePath(final HttpServletRequest request) {
        final String contextPath = request.getContextPath();
        return contextPath.length() > 0 ? contextPath : "/";
    }

    private final String series;

    private RememberMeCookie(final String series) {
        this.series = Objects.requireNonNull(series, "Empty series");
    }

    public String getSeries() {
        return series;
    }

    public Cookie asHttpCookie(final HttpServletRequest request) {
        final Cookie cookie = new Cookie(HTTP_COOKIE_NAME, encodeHttpCookieValue());
        cookie.setMaxAge((int)TTL.getSeconds());
        cookie.setPath(getCookiePath(request));
        cookie.setSecure(request.isSecure());
        cookie.setHttpOnly(true);

        return cookie;
    }

    private String encodeHttpCookieValue() {
        final String base64 = new String(Base64.encode(this.series.getBytes()));

        return StringUtils.trimTrailingCharacter(base64, '=');
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("series", series).toString();
    }
}
