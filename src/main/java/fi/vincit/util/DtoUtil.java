package fi.vincit.util;

import fi.vincit.feature.common.exception.RevisionConflictException;
import fi.vincit.feature.common.BaseEntity;
import fi.vincit.feature.common.BaseEntityDTO;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Objects;

public final class DtoUtil {

    private DtoUtil() {
        throw new AssertionError();
    }

    public static <ID extends Serializable> void assertNoVersionConflict(
            @Nonnull final BaseEntity<ID> entity, @Nonnull final BaseEntityDTO<ID> dto) {

        if (hasVersionConflict(entity, dto)) {
            final String msg =
                    String.format("Update is in conflict with current resource version. %s:%s:%s, %s:%s:%s",
                            entity.getClass().getSimpleName(),
                            entity.getId(),
                            entity.getConsistencyVersion(),
                            dto.getClass().getSimpleName(),
                            dto.getId(),
                            dto.getRev());
            throw new RevisionConflictException(msg);
        }
    }

    public static <ID extends Serializable> boolean hasVersionConflict(
            @Nonnull final BaseEntity<ID> entity, @Nonnull final BaseEntityDTO<ID> dto) {

        Objects.requireNonNull(entity, "entity must not be null");
        Objects.requireNonNull(dto, "dto must not be null");

        return hasVersionConflict(entity, dto.getRev());
    }

    // Protection for update race-conditions using DTO revision number
    public static <ID extends Serializable> boolean hasVersionConflict(
            @Nonnull final BaseEntity<ID> entity, @Nullable final Integer rev) {

        Objects.requireNonNull(entity, "entity must not be null");

        if (rev == null || entity.getConsistencyVersion() == null) {
            // Cannot check without version information
            return false;
        }

        // Persisted entity should have earlier
        return entity.getConsistencyVersion() > rev;
    }

    public static <ID extends Serializable> void copyBaseFields(
            @Nonnull final BaseEntity<ID> entity, @Nonnull final BaseEntityDTO<ID> dto) {

        Objects.requireNonNull(entity, "entity must not be null");
        Objects.requireNonNull(dto, "dto must not be null");

        dto.setId(entity.getId());
        dto.setRev(entity.getConsistencyVersion());
    }
}
