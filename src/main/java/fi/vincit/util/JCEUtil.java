package fi.vincit.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

public final class JCEUtil {
    private static final Logger LOG = LoggerFactory.getLogger(JCEUtil.class);

    public static void removeJavaCryptographyAPIRestrictions() {
        try {
            Field field = Class.forName("javax.crypto.JceSecurity").
                    getDeclaredField("isRestricted");
            field.setAccessible(true);
            field.set(null, Boolean.FALSE);
        } catch (Exception ex) {
            LOG.error("Could not remove JCE restrictions", ex);
        }
    }

    private JCEUtil() {
        throw new AssertionError();
    }
}
