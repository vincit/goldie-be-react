package fi.vincit.config.hibernate;

import fi.vincit.util.EnumUtils;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;
import org.springframework.util.ClassUtils;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.EnumSet;
import java.util.Properties;

/**
 * Store EnumSet as 64 bit vector using long integer under the hood.
 * Enumeration elements are mapped to bits using ordinal position.
 * <p/>
 * NOTE: This solution has some pros and cons.
 * + Space-efficient and fast storage.
 * - Values in database are not human-readable.
 * - Queries for bit-vectors are extremely difficult.
 * - Updates to enumeration values should be avoided because of the ordinal.
 */
public class BinaryEnumSetUserType<E extends Enum<E>> implements ParameterizedType, UserType {

    private Class<E> clazz;

    @SuppressWarnings("unchecked")
    @Override
    public void setParameterValues(Properties parameters) {
        final String enumClassName = parameters.getProperty("enumClass");

        if (enumClassName == null) {
            throw new MappingException("enumClass parameter not specified");
        }

        try {
            this.clazz = (Class<E>) ClassUtils.forName(enumClassName, ClassUtils.getDefaultClassLoader());

        } catch (ClassNotFoundException ex) {
            throw new MappingException("enumClass " + enumClassName + " not found.", ex);
        } catch (ClassCastException ex) {
            throw new MappingException("enumClass " + enumClassName + " is not a Java 5 Enum.", ex);
        }

        if (clazz.getEnumConstants().length > 63) {
            throw new MappingException("enumClass" + enumClassName + " exceeded the maximum of 63 enums.");
        }
    }

    @Override
    public int[] sqlTypes() {
        return new int[]{Types.INTEGER};
    }

    @Override
    public Class returnedClass() {
        return EnumSet.class;
    }

    @Override
    public boolean equals(Object a, Object b) throws HibernateException {
        return a == b || a != null && a.equals(b);
    }

    @Override
    public int hashCode(Object o) throws HibernateException {
        return o.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor sessionImplementor, Object o) throws HibernateException, SQLException {
        long value = resultSet.getLong(names[0]);

        return resultSet.wasNull()
                ? EnumSet.noneOf(clazz)
                : EnumUtils.decodeFromBitVector(clazz, value);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void nullSafeSet(PreparedStatement preparedStatement, Object o, int i, SessionImplementor sessionImplementor) throws HibernateException, SQLException {
        if (o != null) {
            preparedStatement.setLong(i, EnumUtils.encodeAsBitVector((EnumSet<E>) o));
        } else {
            preparedStatement.setLong(i, 0);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object deepCopy(Object o) throws HibernateException {
        return EnumSet.copyOf((EnumSet<E>) o);
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Serializable disassemble(Object o) throws HibernateException {
        return o == null ? null : EnumSet.copyOf((EnumSet<E>) o);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object assemble(Serializable cached, Object o) throws HibernateException {
        return cached == null ? null : EnumSet.copyOf((EnumSet<E>) o);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return EnumSet.copyOf((EnumSet<E>) original);
    }

}
