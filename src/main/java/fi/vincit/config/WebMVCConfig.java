package fi.vincit.config;

import fi.vincit.config.jackson.CustomJacksonObjectMapper;
import fi.vincit.controller.LoggingHandlerExceptionResolver;
import fi.vincit.feature.RuntimeEnvironmentUtil;
import net.rossillo.spring.web.mvc.CacheControlHandlerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.CacheControl;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.annotation.ResponseStatusExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

@EnableWebMvc
@EnableSpringDataWebSupport
@ComponentScan(Constants.CONTROLLER_BASE_PACKAGE)
@Configuration
public class WebMVCConfig extends WebMvcConfigurerAdapter {
    @Resource
    private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    @Bean(name = "multipartResolver")
    public MultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }

    @Bean
    public ViewResolver viewResolver() {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();

        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");

        return resolver;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/admin/hibernate");
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        /*
         * Ensures that dispatcher servlet can be mapped to '/' and that static resources
         * are still served by the containers default servlet.
         */
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        final CacheControl oneYear = CacheControl.maxAge(365, TimeUnit.DAYS).cachePublic();

        registry.addResourceHandler("/favicon.ico")
                .addResourceLocations("/favicon.ico")
                .setCacheControl(oneYear);

        for (final String prefix : Arrays.asList("/js", "/css", "/images", "/fonts", "/i18n")) {
            registry.addResourceHandler(prefix + "/**")
                    .addResourceLocations(prefix + "/")
                    .setCacheControl(oneYear);
        }
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // Cache control handler interceptor to assign cache-control headers to HTTP responses.
        final CacheControlHandlerInterceptor cacheControlHandlerInterceptor = new CacheControlHandlerInterceptor();
        cacheControlHandlerInterceptor.setUseExpiresHeader(true);

        registry.addInterceptor(cacheControlHandlerInterceptor);
    }

    // @see org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
        final StringHttpMessageConverter stringHttpMessageConverter =
                new StringHttpMessageConverter(Constants.DEFAULT_CHARACTER_SET);
        stringHttpMessageConverter.setWriteAcceptCharset(false);

        messageConverters.add(new ByteArrayHttpMessageConverter());
        messageConverters.add(stringHttpMessageConverter);
        messageConverters.add(new ResourceHttpMessageConverter());
        messageConverters.add(new SourceHttpMessageConverter<>());
        messageConverters.add(new AllEncompassingFormHttpMessageConverter());
        messageConverters.add(jacksonMessageConverter());
        messageConverters.add(new Jaxb2RootElementHttpMessageConverter());
    }

    // @see org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport
    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
        // NOTE: Do not change the order! Only first resolution is used.

        // By default Spring MVC does not log exceptions from @Controller methods.
        if (!runtimeEnvironmentUtil.isIntegrationTestEnvironment()) {
            exceptionResolvers.add(new LoggingHandlerExceptionResolver());
        }
        // Process exception using @ExceptionHandler method inside @Controller
        exceptionResolvers.add(exceptionHandlerExceptionResolver());
        // Return status code specified using @ResponseStatus in exception class
        exceptionResolvers.add(new ResponseStatusExceptionResolver());
        // Send HTTP status code depending on the exception which is mapped to error page in web.xml
        exceptionResolvers.add(new DefaultHandlerExceptionResolver());
    }

    @Bean
    public HandlerExceptionResolver exceptionHandlerExceptionResolver() {
        final ExceptionHandlerExceptionResolver exceptionHandlerExceptionResolver = new ExceptionHandlerExceptionResolver();
        exceptionHandlerExceptionResolver.setPreventResponseCaching(true);
        exceptionHandlerExceptionResolver.getMessageConverters().add(jacksonMessageConverter());

        return exceptionHandlerExceptionResolver;
    }

    @Bean
    public MappingJackson2HttpMessageConverter jacksonMessageConverter() {
        final CustomJacksonObjectMapper objectMapper = new CustomJacksonObjectMapper(runtimeEnvironmentUtil);
        final MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonMessageConverter.setObjectMapper(objectMapper);

        return jsonMessageConverter;
    }
}
