package fi.vincit.config.properties;

import com.google.common.base.Throwables;
import fi.vincit.config.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Properties;

/**
 * This ContextInitializer loads properties in environment override file,
 * if such file is specified in build profile. File should be used to store sensitive
 * credentials such as the database password on production server.
 */
public class OverrideConfigInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    private static final Logger LOG = LoggerFactory.getLogger(OverrideConfigInitializer.class);

    private static final String PATH_APPLICATION_PROPERTIES = "configuration/application.properties";
    private static final String PROPERTY_ENVIRONMENT_CONFIG = "environment.override";

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        final ConfigurableEnvironment configurableEnvironment = applicationContext.getEnvironment();

        try {
            final FileSystemResource overrideResource = getEnvironmentOverrideResource();

            if (overrideResource == null) {
                LOG.info("Override configuration location is not specified");

            } else if (overrideResource.exists() && overrideResource.isReadable()) {
                LOG.info("Trying to load properties from: {}", overrideResource.getPath());

                configurableEnvironment.getPropertySources().addFirst(
                        new ResourcePropertySource("overrideConfig", overrideResource));
            } else {
                LOG.warn("Could not open properties file: {}", overrideResource.getPath());
            }

        } catch (IOException e) {
            Throwables.propagate(e);
        }
    }

    private static FileSystemResource getEnvironmentOverrideResource() throws IOException {
        final Properties properties = PropertiesLoaderUtils.loadAllProperties(PATH_APPLICATION_PROPERTIES);
        final String fileName = properties.getProperty(PROPERTY_ENVIRONMENT_CONFIG);

        return StringUtils.hasText(fileName) ? new FileSystemResource(fileName) : null;
    }
}
