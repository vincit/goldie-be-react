package fi.vincit.config.properties;

import fi.vincit.config.Constants;
import fi.vincit.config.hibernate.CustomHibernateNamingStrategy;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@PropertySource("classpath:configuration/jpa.properties")
public class JPAProperties {
    @Value("${jpa.platform}")
    private String jpaPlatform;

    @Value("${hibernate.show_sql}")
    private boolean showSql;

    @Value("${hibernate.format_sql}")
    private boolean formatSql;

    @Value("${hibernate.generate_statistics}")
    private boolean generateStatistics;

    public String getDatabasePlatform() {
        return jpaPlatform;
    }

    // @see org.hibernate.cfg.AvailableSettings
    public Map<String, Object> build() {
        final Map<String, Object> props = new HashMap<>();

        props.put("hibernate.connection.charSet", Constants.DEFAULT_CHARACTER_ENCODING);
        props.put(AvailableSettings.SHOW_SQL, showSql);
        props.put(AvailableSettings.FORMAT_SQL, formatSql);
        props.put(AvailableSettings.GENERATE_STATISTICS, generateStatistics);

        // Disable session metrics logging (when statistics are enabled)
        props.put(AvailableSettings.LOG_SESSION_METRICS, false);

        // Automatically map column names using underscores
        props.put(org.hibernate.jpa.AvailableSettings.NAMING_STRATEGY, CustomHibernateNamingStrategy.class.getName());

        // Pessimistic locking for at most 15 sec (default = wait forever)
        props.put(org.hibernate.jpa.AvailableSettings.LOCK_TIMEOUT, "15000");

        // Sets a maximum "depth" for the outer join fetch tree for single-ended associations (one-to-one, many-to-one).
        // A 0 disables default outer join fetching. e.g. recommended values between 0 and 3
        props.put(AvailableSettings.MAX_FETCH_DEPTH, 1);

        // A non-zero value enables use of JDBC2 batch updates by Hibernate. e.g. recommended values between 5 and 30
        props.put(AvailableSettings.STATEMENT_BATCH_SIZE, 30);

        // Set this property to true if your JDBC driver returns correct row counts from executeBatch().
        // It is usually safe to turn this option on. Hibernate will then
        // use batched DML for automatically versioned data. Defaults to false.
        props.put(AvailableSettings.BATCH_VERSIONED_DATA, true);

        // Forces Hibernate to order SQL updates by the primary key value of the items being updated.
        // This will result in fewer transaction deadlocks in highly concurrent systems.
        props.put(AvailableSettings.ORDER_UPDATES, true);

        // Enable ordering of insert statements for the purpose of more efficient JDBC batching.
        props.put(AvailableSettings.ORDER_INSERTS, true);

        return props;
    }
}
