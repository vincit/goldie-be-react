package fi.vincit.config;

import fi.vincit.config.profile.EmbeddedDatabase;
import fi.vincit.config.profile.StandardDatabase;
import fi.vincit.config.properties.JPAProperties;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.dialect.H2Dialect;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Map;

@Configuration
@Import(JPAProperties.class)
@EnableTransactionManagement(mode = AdviceMode.PROXY, order = AopConfig.ORDER_TRANSACTION)
@EnableJpaRepositories(Constants.FEATURE_BASE_PACKAGE)
public class JPAConfig {

    public static final String[] MODEL_CLASS_PACKAGES = {
            "fi.vincit.feature",
            "org.springframework.data.jpa.convert.threeten"
    };

    @Configuration
    @StandardDatabase
    static class StandardJpaConfiguration {
        @Resource
        protected DataSource dataSource;

        @Resource
        protected JPAProperties jpaProperties;

        @Resource
        protected LiquibaseConfig liquibaseConfig;

        @Bean
        public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
            final LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();

            emf.setJpaDialect(new HibernateJpaDialect());
            emf.setJpaVendorAdapter(jpaVendorAdapter());
            emf.setJpaPropertyMap(jpaProperties());
            emf.setPackagesToScan(MODEL_CLASS_PACKAGES);
            emf.setDataSource(dataSource);

            // Run Liquibase migrations before ORM setup
            liquibaseConfig.upgradeDatabase();

            return emf;
        }

        @Bean
        public HibernateJpaVendorAdapter jpaVendorAdapter() {
            final HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
            jpaVendorAdapter.setDatabasePlatform(getDatabasePlatform());
            return jpaVendorAdapter;
        }

        @Bean
        public JpaTransactionManager transactionManager() {
            return new JpaTransactionManager(entityManagerFactory().getObject());
        }

        @Bean
        public PersistenceExceptionTranslationPostProcessor exceptionTranslationPostProcessor() {
            return new PersistenceExceptionTranslationPostProcessor();
        }

        public String getDatabasePlatform() {
            return jpaProperties.getDatabasePlatform();
        }

        protected Map<String, Object> jpaProperties() {
            return jpaProperties.build();
        }
    }

    @Configuration
    @EmbeddedDatabase
    static class EmbeddedJpaConfiguration extends StandardJpaConfiguration {
        @Override
        public String getDatabasePlatform() {
            return H2Dialect.class.getCanonicalName();
        }

        @Override
        protected Map<String, Object> jpaProperties() {
            final Map<String, Object> jpaProperties = this.jpaProperties.build();
            jpaProperties.put(AvailableSettings.HBM2DDL_AUTO, "update");
            return jpaProperties;
        }
    }
}
