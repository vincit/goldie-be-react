package fi.vincit.config;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public final class Constants {
    public static final String APPLICATION_ROOT_PACKAGE = "fi.vincit";
    public static final String FEATURE_BASE_PACKAGE = "fi.vincit.feature";
    public static final String SECURITY_BASE_PACKAGE = "fi.vincit.security";
    public static final String CONTROLLER_BASE_PACKAGE = "fi.vincit.controller";

    public static final Charset DEFAULT_CHARACTER_SET = StandardCharsets.UTF_8;
    public static final String DEFAULT_CHARACTER_ENCODING = DEFAULT_CHARACTER_SET.name();

    // Spring profiles
    public static final String STANDARD_DATABASE = "standardDatabase";
    public static final String EMBEDDED_DATABASE = "embeddedDatabase";

    public static final String[] DEFAULT_SPRING_PROFILES = new String[]{
            STANDARD_DATABASE
    };

    private Constants() {
        throw new AssertionError();
    }

}
