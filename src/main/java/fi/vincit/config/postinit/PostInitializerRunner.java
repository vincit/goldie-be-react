package fi.vincit.config.postinit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Runner for @PostInitialize annotation registration
 *
 * @author AlphaCSP
 */
@Qualifier("postInitializerRunner")
public class PostInitializerRunner implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostInitializerRunner.class);

    @Override
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        LOGGER.debug("Scanning for Post Initializers...");

        long startTime = System.currentTimeMillis();

        final ApplicationContext applicationContext = event.getApplicationContext();
        final Map<String, Object> beans = applicationContext.getBeansOfType(Object.class, false, false);
        final List<PostInitializingMethod> postInitializingMethods = new LinkedList<>();

        for (final Map.Entry<String, ?> beanEntry : beans.entrySet()) {
            final String beanName = beanEntry.getKey();
            final Object bean = beanEntry.getValue();

            if (bean == null) {
                continue;
            }

            final Class<?> beanClass = bean.getClass();
            final Method[] methods = beanClass.getMethods();

            for (final Method method : methods) {
                if (getAnnotation(method, PostInitialize.class) != null) {
                    if (method.getParameterTypes().length == 0) {
                        final int order = getAnnotation(method, PostInitialize.class).order();
                        postInitializingMethods.add(new PostInitializingMethod(method, bean, order, beanName));
                    } else {
                        LOGGER.warn(
                                "Post Initializer method can't have any arguments. {} in bean {} won't be invoked",
                                method.toGenericString(), beanName);
                    }
                }
            }
        }
        Collections.sort(postInitializingMethods);

        final long endTime = System.currentTimeMillis();

        LOGGER.debug("Application Context scan completed, took {} ms, {} post initializers found. Invoking now.",
                endTime - startTime, postInitializingMethods.size());

        for (final PostInitializingMethod postInitializingMethod : postInitializingMethods) {
            final Method method = postInitializingMethod.getMethod();

            try {
                method.invoke(postInitializingMethod.getBeanInstance());
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new BeanCreationException(
                        "Post Initialization of bean " + postInitializingMethod.getBeanName() + " failed.", e);
            }
        }
    }

    private static <T extends Annotation> T getAnnotation(Method method, final Class<T> annotationClass) {
        do {
            if (method.isAnnotationPresent(annotationClass)) {
                return method.getAnnotation(annotationClass);
            }
            method = getSuperMethod(method);
        } while (method != null);
        return null;
    }

    private static Method getSuperMethod(final Method method) {
        final Class<?> declaring = method.getDeclaringClass();
        if (declaring.getSuperclass() != null) {
            final Class<?> superClass = declaring.getSuperclass();
            try {
                return superClass.getMethod(method.getName(), method.getParameterTypes());
            } catch (NoSuchMethodException | SecurityException e) {
            }
        }
        return null;
    }

    private static class PostInitializingMethod implements Comparable<PostInitializingMethod> {

        private final Method method;
        private final Object beanInstance;
        private final int order;
        private final String beanName;

        private PostInitializingMethod(
                final Method method, final Object beanInstance, final int order, String beanName) {

            this.method = method;
            this.beanInstance = beanInstance;
            this.order = order;
            this.beanName = beanName;
        }

        public Method getMethod() {
            return method;
        }

        public Object getBeanInstance() {
            return beanInstance;
        }

        public String getBeanName() {
            return beanName;
        }

        @Override
        public int compareTo(final PostInitializingMethod anotherPostInitializingMethod) {
            final int thisVal = this.order;
            final int anotherVal = anotherPostInitializingMethod.order;
            return thisVal < anotherVal ? -1 : thisVal == anotherVal ? 0 : 1;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }

            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final PostInitializingMethod that = (PostInitializingMethod) o;

            return order == that.order && !(beanName != null ? !beanName.equals(that.beanName) : that.beanName != null)
                    && !(method != null ? !method.equals(that.method) : that.method != null);
        }

        @Override
        public int hashCode() {
            int result;
            result = method != null ? method.hashCode() : 0;
            result = 31 * result + (beanInstance != null ? beanInstance.hashCode() : 0);
            result = 31 * result + order;
            result = 31 * result + (beanName != null ? beanName.hashCode() : 0);
            return result;
        }
    }

}
