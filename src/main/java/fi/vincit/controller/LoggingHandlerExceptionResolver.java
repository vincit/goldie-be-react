package fi.vincit.controller;

import com.google.common.base.Throwables;
import fi.vincit.feature.common.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class LoggingHandlerExceptionResolver implements HandlerExceptionResolver {
    private static final Logger LOG = LoggerFactory.getLogger(LoggingHandlerExceptionResolver.class);

    @Override
    public ModelAndView resolveException(HttpServletRequest request,
                                         HttpServletResponse response,
                                         Object handler,
                                         Exception ex) {
        if (ex instanceof NotFoundException) {
            LOG.error("Requested resource was not found for URI={} method={} message={}",
                    request.getRequestURI(), request.getMethod(), ex.getMessage());
            return null;

        } else if (ex instanceof NoSuchRequestHandlingMethodException ||
                ex instanceof HttpRequestMethodNotSupportedException) {
            LOG.error("Handler method not found for URI={} method={}",
                    request.getRequestURI(), request.getMethod());
            return null;
        }

        final ResponseStatus responseStatus = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class);

        if (responseStatus != null) {
            LOG.error("Caught exception with @ResponseStatus {} with message {} for handler {}",
                    ex.getClass().getSimpleName(), ex.getMessage(), getHandlerName(handler));
        } else {
            LOG.error("Handler execution resulted in exception", Throwables.getRootCause(ex));
        }

        return null;
    }

    private static String getHandlerName(Object handler) {
        if (handler != null) {
            if (handler instanceof HandlerMethod) {
                final HandlerMethod handlerMethod = (HandlerMethod) handler;
                final Object bean = handlerMethod.getBean();
                final Method method = handlerMethod.getMethod();

                return bean.getClass().getName() + "." + method.getName();
            }
        }

        return "?";
    }
}
