package fi.vincit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Common implementations of @ExceptionHandler, @InitBinder and @ModelAttribute
 * for all @Controllers.
 */
@ControllerAdvice
public class CommonControllerAdvice {
    /**
     * All auto-detected validators.
     */
    @Autowired
    private Collection<Validator> validators;

    /**
     * Common implementation of @IniBinder for all @Controllers
     *
     * @param binder Contains binding target and validators.
     */
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));

        Object target = binder.getTarget();
        if (target != null) {
            binder.replaceValidators(supportedValidatorsFor(target.getClass()));
        }
    }

    /**
     * Get all supported validators for given Class.
     *
     * @param clazz Class of which validators are fetched
     * @return Array of validators supporting given Class
     */
    private Validator[] supportedValidatorsFor(Class<?> clazz) {
        // Use Set to filter out duplicates
        Set<Validator> supportedValidators = new HashSet<>();
        for (Validator validator : validators) {
            if (validator.supports(clazz)) {
                supportedValidators.add(validator);
            }
        }
        return supportedValidators.toArray(new Validator[supportedValidators.size()]);
    }
}
