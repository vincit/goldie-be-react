package fi.vincit.controller.api;

import fi.vincit.feature.account.AccountDTO;
import fi.vincit.feature.account.AccountEditFeature;
import fi.vincit.feature.account.password.change.ChangePasswordDTO;
import net.rossillo.spring.web.mvc.CacheControl;
import net.rossillo.spring.web.mvc.CachePolicy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.validation.Valid;

@RestController
@RequestMapping(value = AccountApiResource.BASE_URI, produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountApiResource {
    public static final String BASE_URI = "/api/v1/account";

    @Inject
    private AccountEditFeature accountEditFeature;

    /**
     * GET  /account -> get the current user.
     */
    @CacheControl(policy = CachePolicy.NO_CACHE)
    @RequestMapping(method = RequestMethod.GET)
    public AccountDTO getAccount() {
        return accountEditFeature.getActiveAccount();
    }

    /**
     * PUT, POST, PATCH  /account -> update the current user information.
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void saveAccount(@RequestBody @Valid AccountDTO dto) {
        accountEditFeature.updateActiveAccount(dto);
    }

    /**
     * POST  /account/password -> changes the current user's password
     */
    @RequestMapping(value = "password", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void changePassword(@RequestBody @Valid ChangePasswordDTO dto) {
        accountEditFeature.changeActiveUserPassword(dto);
    }
}
