package fi.vincit.controller.api;

import fi.vincit.feature.account.user.SystemUserCrudFeature;
import fi.vincit.feature.account.user.SystemUserDTO;
import net.rossillo.spring.web.mvc.CacheControl;
import net.rossillo.spring.web.mvc.CachePolicy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/admin/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserApiResource {
    /**
     * @apiDefine UserResourceSuccess
     * @apiSuccess {String} firstname Firstname of the User.
     * @apiSuccess {String} lastname  Lastname of the User.
     * @apiSuccess {String} role Role of the User
     * @apiSuccess {String} email Contact email of the User
     * @apiSuccess {String} timeZone Preferred local timezone of the User
     * @apiSuccess {String} locale Locale of the user for i18n
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "id": "1",
     *       "rev": "0",
     *       "active": true,
     *       "role": "ROLE_USER",
     *       "username": "user",
     *       "email": "user@invalid",
     *       "firstname": "John",
     *       "lastname": "Doe",
     *       "timeZone": "Europe/Helsinki",
     *       "locale": "fi-FI"
     *     }
     */

    /**
     * @apiDefine RequiresAdminPermission User access only
     * This optional description belong to to the group admin.
     */

    /**
     * @apiDefine UserNotFoundError
     * @apiError(404) UserNotFoundError The id of the User was not found.
     */

    /**
     * @apiDefine RevisionConflictError Revision conlict
     * @apiError (409) RevisionConflictError The revision of the stored resource is newer than given in the request.
     */

    /**
     * @apiDefine PasswordDoesNotMatchError Password does not match confirmation
     * @apiError (403) PasswordDoesNotMatchError The given password confirmation does not match.
     */

    @Resource
    private SystemUserCrudFeature crudFeature;

    /**
     * @api {get} /api/v1/admin/users List of Users
     * @apiName ListUsers
     * @apiGroup User
     * @apiPermission RequiresAdminPermission
     * @apiParam {Number} [page=0] page to be returned counting from 0
     * @apiParam {Number} [size=10] number of items to be returned
     * @apiParam {String} [sort=username,ASC] sort option and direction
     */
    @CacheControl(policy = CachePolicy.NO_CACHE)
    @RequestMapping(method = RequestMethod.GET)
    public List<SystemUserDTO> list(
            @PageableDefault(size = 10)
            @SortDefault(sort = "username", direction = Sort.Direction.ASC) Pageable pageRequest) {
        return crudFeature.list(pageRequest).getContent();
    }

    /**
     * @api {get} /api/v1/admin/users?type=page List page of Users
     * @apiName ListUsersPage
     * @apiGroup User
     * @apiPermission RequiresAdminPermission
     * @apiParam {Number} [page=0] page to be returned counting from 0
     * @apiParam {Number} [size=10] number of items to be returned
     * @apiParam {String} [sort=username,ASC] sort option and direction
     */
    @CacheControl(policy = CachePolicy.NO_CACHE)
    @RequestMapping(method = RequestMethod.GET, params = "type=page")
    public Page<SystemUserDTO> page(Pageable pageRequest) {
        return crudFeature.list(pageRequest);
    }

    /**
     * @api {get} /api/v1/admin/users/:id Get User
     * @apiName GetUser
     * @apiGroup User
     * @apiPermission RequiresAdminPermission
     * @apiParam {Number} id Users unique ID.
     * @apiUse UserResourceSuccess
     * @apiUse UserNotFoundError
     */
    @CacheControl(policy = CachePolicy.NO_CACHE)
    @RequestMapping(value = "{id:\\d+}", method = RequestMethod.GET)
    public SystemUserDTO read(@PathVariable Long id) {
        return crudFeature.read(id);
    }

    /**
     * @api {post} /api/v1/admin/users Create user
     * @apiName CreateUser
     * @apiGroup User
     * @apiPermission RequiresAdminPermission
     * @apiUse PasswordDoesNotMatchError
     * @apiParam {String} password Password for the user
     * @apiParam {String} passwordConfirm Confirmation for the password
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public SystemUserDTO create(@RequestBody @Validated(SystemUserDTO.Create.class) SystemUserDTO dto) {
        return crudFeature.create(dto);
    }

    /**
     * @api {put} /api/v1/admin/users/:id Update user
     * @apiName UpdateUser
     * @apiGroup User
     * @apiPermission RequiresAdminPermission
     * @apiUse UserNotFoundError
     * @apiUse RevisionConflictError
     * @apiUse PasswordDoesNotMatchError
     */
    @RequestMapping(value = "{id:\\d+}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public SystemUserDTO update(@PathVariable Long id, @RequestBody @Validated(SystemUserDTO.Edit.class) SystemUserDTO dto) {
        dto.setId(id);

        return crudFeature.update(dto);
    }

    /**
     * @api {delete} /api/v1/admin/users/:id Delete User
     * @apiName DeleteUser
     * @apiGroup User
     * @apiPermission RequiresAdminPermission
     * @apiUse UserNotFoundError
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{id:\\d+}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        crudFeature.delete(id);
    }
}
