package fi.vincit.controller.api;

import com.google.common.collect.ImmutableMap;
import fi.vincit.feature.common.exception.NotFoundException;
import fi.vincit.feature.common.exception.NotImplementedException;
import fi.vincit.feature.storage.FileStorageService;
import fi.vincit.feature.storage.metadata.FileType;
import fi.vincit.feature.storage.metadata.PersistentFileMetadata;
import net.rossillo.spring.web.mvc.CacheControl;
import net.rossillo.spring.web.mvc.CachePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/test")
public class TestApiResource {
    private static final Logger LOG = LoggerFactory.getLogger(TestApiResource.class);

    @Resource
    private FileStorageService fileStorageService;

    @CacheControl(policy = CachePolicy.NO_CACHE)
    @RequestMapping(value = "/hello", produces = "text/plain")
    public String hello() {
        return "Hello World!";
    }

    @RequestMapping(value = "/helloPOST", method = RequestMethod.POST, produces = "text/plain")
    public String helloPOST(@RequestBody String body) {
        return "Hello " + body + "!";
    }

    @CacheControl(policy = CachePolicy.NO_CACHE)
    @RequestMapping(value = "/helloJSON", produces = "application/json")
    public Map<String, Object> helloJSON() {
        return ImmutableMap.<String, Object>builder()
                .put("status", "SUCCESS")
                .put("result", "Hello World!")
                .build();
    }

    @CacheControl(policy = CachePolicy.NO_CACHE)
    @RequestMapping(value = "/secure/hello", produces = "text/plain")
    public String helloSecure() {
        return "Hello World!";
    }

    @RequestMapping(value = "/throw")
    public String throwTestException(@RequestParam(required = false) String type) {
        if ("state".equals(type)) {
            throw new IllegalStateException("state");
        } else if (type.equals("argument")) {
            throw new IllegalArgumentException("argument");
        } else if (type.equals("notfound")) {
            throw new NotFoundException("notfound");
        } else if (type.equals("notimplemented")) {
            throw new NotImplementedException("notimplemented");
        } else if (type.equals("accessdenied")) {
            throw new AccessDeniedException("accessdenied");
        } else {
            throw new RuntimeException("unknown");
        }
    }

    @Transactional
    @RequestMapping(value = "/file", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public UUID uploadFile(@RequestPart MultipartFile multipartFile) throws IOException {
        final UUID uuid = UUID.randomUUID();
        fileStorageService.storeFile(uuid, multipartFile, FileType.TEST_DB);
        return uuid;
    }

    @Transactional(readOnly = true)
    @RequestMapping(value = "/file/{uuid}", method = RequestMethod.GET)
    public ResponseEntity<?> downloadFile(@PathVariable UUID uuid) throws IOException {
        try {
            if (fileStorageService.exists(uuid)) {
                final PersistentFileMetadata metadata = fileStorageService.getMetadata(uuid);
                final ResponseEntity.BodyBuilder builder = ResponseEntity.ok();

                if (metadata.getContentType() != null) {
                    builder.contentType(MediaType.parseMediaType(metadata.getContentType()));
                }

                if (metadata.getOriginalFileName() != null) {
                    builder.header(HttpHeaders.CONTENT_DISPOSITION,
                            "attachment; filename=\"" + metadata.getOriginalFileName() + "\"");
                }

                return builder.body(fileStorageService.getBytes(uuid));
            } else {
                return ResponseEntity.notFound().build();
            }

        } catch (IllegalStateException | IllegalArgumentException e) {
            LOG.error("Error downloading file", e);
            throw new NotFoundException();
        }
    }

    @Transactional
    @RequestMapping(value = "/file/{uuid}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteFile(@PathVariable UUID uuid) throws IOException {
        fileStorageService.remove(uuid);

        return ResponseEntity.noContent().build();
    }
}
