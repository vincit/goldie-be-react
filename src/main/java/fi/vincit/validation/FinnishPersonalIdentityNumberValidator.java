package fi.vincit.validation;

import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class FinnishPersonalIdentityNumberValidator implements ConstraintValidator<FinnishPersonalIdentity, String> {

    private static final char[] CHECK_CHARACTERS = "0123456789ABCDEFHJKLMNPRSTUVWXY".toCharArray();

    private static final Pattern PERSONAL_IDENTITY_NUMBER_PATTERN = Pattern.compile(
            "(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])([0-9]{2})[+-A][0-9]{3}" +
                    String.format("[%s]", String.valueOf(CHECK_CHARACTERS)));

    public FinnishPersonalIdentityNumberValidator() {
    }

    @Override
    public void initialize(FinnishPersonalIdentity constraintAnnotation) {
    }

    @Override
    public boolean isValid(String personalIdentityNumber, ConstraintValidatorContext context) {
        if (personalIdentityNumber == null) {
            return true;
        }

        final Matcher patternMatcher = PERSONAL_IDENTITY_NUMBER_PATTERN.matcher(personalIdentityNumber);
        if (!patternMatcher.matches()) {
            return false;
        }

        if (!isBirthDateValid(patternMatcher, personalIdentityNumber)) {
            return false;
        }

        return checkCharacterMatches(personalIdentityNumber);
    }

    /**
     * The birth date in the Finnish personal identification number must be a valid date before tomorrow,
     * Finnish personal identification numbers are never issued in advance.
     *
     * Also any birth dates before 1850 are not considered valid (no one born before 1850 has been issued
     * a Finnish identification number).
     *
     * @return true is the date is valid for Finnish personal identification number and false otherwise
     */
    private static boolean isBirthDateValid(final Matcher patternMatcher, String personalIdentityNumber) {
        final int dayOfMonth = Integer.parseInt(patternMatcher.group(1));
        final int monthOfYear = Integer.parseInt(patternMatcher.group(2));
        final int year = getCentury(personalIdentityNumber) + Integer.parseInt(patternMatcher.group(3));

        boolean isBirthDateValid;
        try {
            LocalDate birthDate = LocalDate.of(year, monthOfYear, dayOfMonth);
            isBirthDateValid = year >= 1850
                    && birthDate.getYear() == year
                    && birthDate.getMonthValue() == monthOfYear
                    && birthDate.getDayOfMonth() == dayOfMonth
                    && birthDate.isBefore(LocalDate.now().plusDays(1));
        } catch (DateTimeException e) {
            isBirthDateValid = false;
        }
        return isBirthDateValid;
    }

    private static int getCentury(String personalIdentityNumber) {
        final char centuryChar = personalIdentityNumber.charAt(6);
        return centuryChar == '+' ? 1800 : centuryChar == '-' ? 1900 : centuryChar == 'A' ? 2000 : 0;
    }

    private static boolean checkCharacterMatches(String personalIdentityNumber) {
        final String birthDate = personalIdentityNumber.substring(0, 6);
        final String identityNumber = personalIdentityNumber.substring(7, 10);
        final int checkSum = Integer.parseInt(birthDate + identityNumber) % 31;
        final char checkCharacter = personalIdentityNumber.charAt(10);
        return checkCharacter == CHECK_CHARACTERS[checkSum];
    }
}
