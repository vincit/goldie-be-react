package fi.vincit.validation;


import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber, String> {
    private String defaultRegion;

    @Override
    public void initialize(PhoneNumber constraintAnnotation) {
        this.defaultRegion = constraintAnnotation.defaultRegion();
    }

    @Override
    public boolean isValid(String number, ConstraintValidatorContext context) {
        if (!StringUtils.hasText(number)) {
            return true;
        }

        try {
            final PhoneNumberUtil util = PhoneNumberUtil.getInstance();

            return util.isValidNumber(util.parse(number, defaultRegion));

        } catch (NumberParseException ex) {
            return false;
        }
    }
}
