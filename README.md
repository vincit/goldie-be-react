Goldie Backend with React
=========================

## Info
This is a fork of the original Goldie Backend project. The backend is the
same, but the Angular frontend, that the Goldie Backend project had implemented,
has been remade here with React and its features. 

To run the frontend in development mode, consult the ```README.md``` inside the ```frontend``` directory.

This was done to ease the start of development with using React and for 
example in the Valo/Urheilupalvelu project the features can act as the
basis of the project. Also, Juho Sassali created the React Seed project, that includes examples on how to get started with React.
[https://bitbucket.org/vincit/react-seed](https://bitbucket.org/vincit/react-seed)

## Developer profile
Developer specific profile file  
Either comment out the following section from pom.xml

	<build>
		<filters>  
			<!-- Enable this for developer specific properties. -->
			<filter>profiles/${build.profile.id}/config.${user.name}.properties</filter>
		</filters>
	</build>

or create new file,`{{projectDirectory}}\profiles\dev\config.{{username}}.properties`, containing your personal parameter overrides.

## Default user accounts
admin/admin   
user/user

## Database support

To make native UUID work on PG use JDBC parameter: stringtype=unspecified
see. https://github.com/pgjdbc/pgjdbc/issues/247

QueryDSL types for native SQL query are generated manually from target database schema using Maven target querydsl:export

## JVM parameters
To avoid JVM OutOfMemory errors then at least the following JVM options must be applied for Tomcat and Maven:

-Xms384m -Xmx512m -XX:+UseConcMarkSweepGC

Some additional tweaks and more memory are recommended for production use:

-Xms512m -Xmx1024m -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true

## Prerequisite

[Maven](http://maven.apache.org/)  
Git ([msysGit for Windows](http://msysgit.github.io/))  
[Node.js](http://nodejs.org/download/)  
[Bower](http://bower.io/) (npm install -g bower)  
[Gulp](http://gulpjs.com/) (npm install -g gulp)

## Admin tools

For hibernate statistics enable profile parameter hibernate.generate_statistics=true and browse to
http://localhost:9070/admin/hibernate

## Notes for Eclipse users

Tested with Eclipse Luna Java EE version

- Import existing Maven project -> Eclipse should open dialog "Setup Maven plugin connectors" and let you install buildhelper connector automatically. Select "resolve later" for antrun.
- Install antrun connector: https://code.google.com/p/nl-mwensveen-m2e-extras/
- Install Tomcat and add Tomcat/lib/jsp-api.jar and servlet-api.jar to java build path / libraries.

## Known issues
