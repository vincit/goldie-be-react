# -*- mode: ruby -*-
# vi: set ft=ruby :

PROJECT_NAME = "goldie-be"

Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/trusty64"

  if USER == 'jenkins' or USER == 'ci'
    config.vm.network :forwarded_port, guest: 9070, host: 9070, gateway_ports: true, host_ip: "*"
  else
    config.vm.network :forwarded_port, guest: 8080, host: 4880
    config.vm.network :private_network, ip: '192.168.33.10'
  end

  # Hostname of the system: note the custom username from `id -u -n`
  config.vm.hostname = "#{PROJECT_NAME}-vagrant-#{`id -u -n|tr -d '\n'`}"

  if ENV['VAGRANT_DEFAULT_PROVIDER'] == 'libvirt'
    config.ssh.password = "vagrant"
    config.vm.network :public_network, :dev => "virbr0", :mode => "bridge", :type => "bridge"

    config.vm.synced_folder ".", "/vagrant", type: "9p"
    config.vm.synced_folder "/tmp/vagrant/cache/#{PROJECT_NAME}/.m2", "/home/vagrant/.m2", type: "9p", create: true
    config.vm.synced_folder "/tmp/vagrant/cache/#{PROJECT_NAME}/.npm", "/home/vagrant/.npm", type: "9p", create: true
    config.vm.synced_folder "/tmp/vagrant/cache/#{PROJECT_NAME}/.cache/bower", "/home/vagrant/.cache/bower", type: "9p", create: true
  else
    config.vm.synced_folder "/tmp/vagrant/cache/#{PROJECT_NAME}/.m2", "/home/vagrant/.m2", create: true
    config.vm.synced_folder "/tmp/vagrant/cache/#{PROJECT_NAME}/.npm", "/home/vagrant/.npm", create: true
    config.vm.synced_folder "/tmp/vagrant/cache/#{PROJECT_NAME}/.cache/bower", "/home/vagrant/.cache/bower", create: true
  end

  config.vm.provider :virtualbox do |vb|
    vb.cpus = 2
    vb.memory = 2048
    vb.customize ["modifyvm", :id, "--nictype1", "virtio"]
    vb.customize ["modifyvm", :id, "--hwvirtex", "on"]
  end

  config.vm.provider :libvirt do |domain|
    domain.cpus = 2
    domain.memory = 2048
    domain.random_hostname = true
  end

  config.vm.provision :ansible do |ansible|
    ansible.playbook = "ansible/provision.yml"
  end
end
